<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class System_model extends CI_Model {

	/*
	* package: model.system_model
	* date: 21-08-2013
	* by: Fernando
	*/
	function __construct()
    {
        // Método construtor da classe pai
        parent::__construct();
        $this->load->database();
    }

    /*
    * Insert
    * Insere novos dados
    * @param @table, $arrParam
    */
    function insert($table, $arrParam)
	{
		$result = $this->db->insert($table, $arrParam);
		return $result;
	}

	/*
	* Update
	* Realiza o update de dados
	* @param @table, $arrParam $where
	*/
    function update($table, $arrParam, $where)
	{
		$result = $this->db->update($table, $arrParam, $where);
		return $result;
	}


	/*
	* SELECT__WHERE
	* retorna dados de uma tabela
	*/
	function select_where($table, $where)
	{
		$sql = sprintf("SELECT * FROM %s WHERE %s", $table, $where);

		$dados = $this->db->query($sql);

		return $dados->result_array();
	}

	/*
	* SELECT__WHERE
	* retorna dados de uma tabela
	*/
	function select_count($table, $where)
	{
		$sql = sprintf("SELECT %s FROM %s WHERE %s", $fields, $table, $where);

		$dados = $this->db->query($sql);

		return $dados->result_array();
	}

	/*
	* Delete
	* Deleta conteúdo
	* @param @table, $where
	*/
	function delete($table, $where)
	{
		$result = $this->db->delete($table, $where);
		return $result;
	}

	/*
	* LAST_ID
	* retorna o ultimo ID inserido
	*/
	function last_id()
	{
		return $this->db->insert_id();
	}

	/*
	 * AFFECTED_ROWS
	 * Total de linhas afetadas na última inserção
	 */
	function affected_rows()
	{
		return $this->db->affected_rows();
	}

		/*
    * method: get_unique_field()
    * params: 	@table => nome da tabela, 
    *			@column => Coluna a ser retornada,
    *			@where => where no formato SQL
    * info: Atualiza os dados passados no array
    */
	function get_unique_field($table, $column, $where)
	{
		$sql   = sprintf("select %s as returnedField from %s where %s", $column, $table, $where);
		$result = $this->db->query($sql);

		if ($result->num_rows() > 0)
		{
			$row = $result->row();
			return $row->returnedField;
		}
		else
		{
			return '';
		}
	}
}
