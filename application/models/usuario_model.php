<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
	CREATE TABLE usuario(
		id int not null auto_increment primary key,
		tipousuario int default 2,
		subdominio varchar(60) not null,
		nome_completo varchar(256) not null,
		creci varchar(256) not null,
		creci_uf char(2) not null,
		data_nascimento date not null,
		email varchar(256) not null,
		senha varchar(50) not null,
		estado char(2) not null,
		cidade varchar(200) not null,
		bairro varchar(200) default null,
		telefone varchar(50) default null,
		celular varchar(50) default null,
		termo char(1) not null,
		data_registro timestamp default current_timestamp,
		visible char(1) default 'S'
	);
*/
class Usuario_model extends CI_Model {

	private $tipousuario = 2;
	private $subdominio = null;
	private $nome_completo = null;
	private $creci = null;
	private $creci_uf = null;
	private $data_nascimento = null;
	private $email = null;
	private $senha = null;
	private $estado = null;
	private $cidade = null;
	private $bairro = null;
	private $telefone = null;
	private $celular = null;
	private $termo = null;

	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	// method @setters
	public function setSubdominio($value){
		$this->subdominio = $value;
	}
	public function setNome($value){
		$this->nome_completo = $value;
	}
	public function setCreci($value){
		$this->creci = $value;
	}
	public function setCreciUF($value){
		$this->creci_uf = $value;
	}
	public function setDataNascimento($value){
		$this->data_nascimento = $value;
	}
	public function setEmail($value){
		$this->email = $value;
	}
	public function setSenha($value){
		$this->senha = md5($value . $this->getEmail());
	}
	public function setEstado($value){
		$this->estado = $value;
	}
	public function setCidade($value){
		$this->cidade = $value;
	}
	public function setBairro($value){
		$this->bairro = $value;
	}
	public function setTelefone($value){
		$this->telefone = $value;
	}
	public function setCelular($value){
		$this->celular = $value;
	}
	public function setTermo($value){
		$this->termo = $value;
	}


	// method @getters
	public function getTipoUsuario(){
		return $this->tipousuario;
	}
	public function getSubdominio(){
		return $this->subdominio;
	}
	public function getNome(){
		return $this->nome_completo;
	}
	public function getCreci(){
		return $this->creci;
	}
	public function getCreciUF(){
		return $this->creci_uf;
	}
	public function getDataNascimento(){
		return $this->data_nascimento;
	}
	public function getEmail(){
		return $this->email;
	}
	public function getSenha(){
		return $this->senha;
	}
	public function getEstado(){
		return $this->estado;
	}
	public function getCidade(){
		return $this->cidade;
	}
	public function getBairro(){
		return $this->bairro;
	}
	public function getTelefone(){
		return $this->telefone;
	}
	public function getCelular(){
		return $this->celular;
	}
	public function getTermo(){
		return $this->termo;
	}


	function save(){

		$param = array(
			'tipousuario' 	=> $this->getTipoUsuario(),
			'subdominio' 	=> $this->getSubdominio(),
			'nome_completo' => $this->getNome(),
			'creci' 		=> $this->getCreci(),
			'creci_uf' 		=> $this->getCreciUF(),
			'data_nascimento' => $this->getDataNascimento(),
			'email' 		=> $this->getEmail(),
			'senha' 		=> $this->getSenha(),
			'estado' 		=> $this->getEstado(),
			'cidade' 		=> $this->getCidade(),
			'bairro' 		=> $this->getBairro(),
			'telefone' 		=> $this->getTelefone(),
			'celular' 		=> $this->getCelular(),
			'termo' 		=> $this->getTermo()
		);

		$this->db->insert('usuario', $param);
		return $this->db->affected_rows();
	}


}