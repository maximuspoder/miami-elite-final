<?php
/**
 * User: fernandoalls
 * Date: 02/02/15
 * Time: 19:38
 */
class Acesso_externo_model extends CI_Model{

	public function  __construct(){
        parent::__construct();
        $this->load->database();
    }

    /**
     * @description Get info about user website
     * @return mixed
     */
    public function getInfo(){

    	$dominio = rtrim(BASEURL, '/');
    	$sql = "SELECT 	count(1) as total,
    					u.id as userid,
						u.*
					FROM users u
					LEFT JOIN properties_relationships_users ru ON (ru.userid = u.id)
					WHERE subdominio = '{$dominio}'";
		$dados = $this->db->query($sql);
        return $dados->result();
        echo $this->db->last_query();
    }

    /**
     * @description Get Homepage photos
     * @param $userid
     * @return mixed
     */
    public function getImoveis(){
    	$sql = "SELECT * 
						FROM featured f
                        INNER JOIN featured_photos p ON (p.iddestaque = f.id)
                        GROUP BY f.id
                        ORDER BY f.id DESC
                        LIMIT 6";;
		$dados = $this->db->query($sql);
        return $dados->result();
    }

    /**
     * @description Get total Items
     * @param $userid
     * @return mixed
     */
    public function getCountImoveis($userid){

        $sql = "SELECT count(1) as total
                        FROM properties_relationships_users ru
                        LEFT JOIN property1 p1 ON (p1.field_157 = ru.mlnumber)
                        LEFT JOIN property2 p2 ON (p2.field_157 = ru.mlnumber)
                        LEFT JOIN property3 p3 ON (p3.field_157 = ru.mlnumber)
                        LEFT JOIN property4 p4 ON (p4.field_157 = ru.mlnumber)
                        LEFT JOIN property5 p5 ON (p5.field_157 = ru.mlnumber)
                        LEFT JOIN property6 p6 ON (p6.field_157 = ru.mlnumber)
                        LEFT JOIN property7 p7 ON (p7.field_157 = ru.mlnumber)
                        LEFT JOIN property8 p8 ON (p8.field_157 = ru.mlnumber)
                        WHERE ru.userid = {$userid}";
        $dados = $this->db->query($sql);
        return $dados->result();
    }

    public function getFeaturedByID($id){
        $sql = "SELECT * 
                        FROM featured f
                        INNER JOIN featured_photos p ON (p.iddestaque = f.id)
                        WHERE f.id = {$id}";
        $dados = $this->db->query($sql);
        return $dados->result();
    }

    public function getPropertyBySYSID($sysid){

        $sql = "SELECT * FROM properties_relationships WHERE sysid = ".$sysid;
        $dados = $this->db->query($sql);
        $x= $dados->result_array();

        $property = strtolower($x[0]['property_type']);


        if($property != 13){
            $table = $property;
        }
        $sql = "SELECT *
                        FROM properties_relationships pr
                        INNER JOIN {$table} p on p.field_157 = pr.mlnumber
                        WHERE pr.sysid = '{$sysid}'";

        $dados = $this->db->query($sql);
        return $dados->result_array();
        echo $this->db->last_query();
    }




    public function getResultByTableProperty($table, $params, $districtMap){

        $where = 'WHERE u.subdominio = "'.BASEURL_SEARCH.'"';
        $where .= " AND property_type = '".$table."'";
        // Search by ML Number
        if($params->mlnumber != ''){
            $where .= "AND ru.mlnumber LIKE '%{$params->mlnumber}%'";
        }

        // Search by Zip Code
        if($params->zipcode != ''){
            $where .= " AND p.field_10 LIKE '%{$params->zipcode}%'";
        }

        // Search by City
        if($params->city != ''){
            $where .= " AND p.field_922 LIKE '%{$params->city}%'";
        }

        // Search by Price
        if($params->price != ''){
            $where .= " AND p.field_137 LIKE '%{$params->price}%'";
        }

        $sql = "SELECT  ru.mlnumber,
                        r.property_type,
                        r.sysid,
                        p.*
                        FROM users u
                        LEFT JOIN properties_relationships_users ru on ru.userid = u.id
                        LEFT JOIN properties_relationships r on r.mlnumber = ru.mlnumber
                        LEFT JOIN {$table} p on p.field_157 = r.mlnumber
                        {$where}
                        GROUP BY ru.mlnumber";


        $dados = $this->db->query($sql);
        return $dados->result();
        echo $this->db->last_query(); die();
    }

}