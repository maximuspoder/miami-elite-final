<?php
/**
 * User: fernandoalls
 * Date: 02/02/15
 * Time: 19:38
 */
class Dashboard_model extends CI_Model{

	public function  __construct(){
        parent::__construct();
        $this->load->database();
    }

    /**
     * @description Set property to user
     * @param $params
     * @return int
     */
    public function ajax_set_property($params){

    	$paramsInsert['mlnumber'] 	= $params->mlnumber;
    	$paramsInsert['userid'] 	= $params->userid;
    	$paramsInsert['created_at'] = date('Y-m-d H:i:s');
    	$paramsInsert['updated_at'] = date('Y-m-d H:i:s');

    	$this->db->insert('properties_relationships_users', $paramsInsert);
        return $this->db->affected_rows();
    }

    /**
     * @description Get user properties
     * @param string $table
     * @param string $property_type
     * @return mixed
     */
    public function get_userStore($table, $property_type){

    	$userid = $this->session->userdata('id');
    	$sql = "SELECT 	*
						FROM properties_relationships_users u
						INNER JOIN properties_relationships r on (r.mlnumber = u.mlnumber)
						LEFT JOIN {$table} p1 on (p1.field_157 = r.mlnumber)
					WHERE u.userid = {$userid}
					AND r.property_type = '{$property_type}'
                    GROUP BY u.mlnumber";

		$dados = $this->db->query($sql);
        return $dados->result();
        echo $this->db->last_query();
    }

    public function get_userStoreByID($table, $property_type, $userid){

        $sql = "SELECT  *
                        FROM properties_relationships_users u
                        INNER JOIN properties_relationships r on (r.mlnumber = u.mlnumber)
                        LEFT JOIN {$table} p1 on (p1.field_157 = r.mlnumber)
                    WHERE u.userid = {$userid}
                    AND r.property_type = '{$property_type}'
                    GROUP BY u.mlnumber";

        $dados = $this->db->query($sql);
        return $dados->result();
        echo $this->db->last_query();
    }

    /**
     * @description remove property of user
     * @param $params
     * @return int
     */
    public function ajax_remove_property($params){
    	$where = array('mlnumber' => $params->mlnumber, 'userid' => $params->userid);

    	$this->db->delete('properties_relationships_users', $where);
    	return $this->db->affected_rows();
    }

    /**
     * @desciption Get User Data
     * @return mixed
     */
    public function userInformation($id){


        $sql = "SELECT *
                        FROM users 
                        WHERE id = {$id}";

        $dados = $this->db->query($sql);
        return $dados->result();
        echo $this->db->last_query();
    }

    public function getFeatured(){
        $sql = "SELECT  *,
                        f.id as featuredid 
                        FROM featured f
                        INNER JOIN featured_photos p on p.iddestaque = f.id
                        GROUP BY f.id
                        ORDER BY f.id DESC
                        LIMIT 6;";
        $dados = $this->db->query($sql);
        return $dados->result();
        echo $this->db->last_query();
    }


    public function getCorretores($id){
        $sql = "SELECT *,
                        (select count(1) from properties_relationships_users pru WHERE pru.userid = u.id) as total
                        FROM users u
                        WHERE u.id <> {$id}
                        ORDER BY u.id ASC";
        $dados = $this->db->query($sql);
        return $dados->result();
        echo $this->db->last_query();
    }

}
