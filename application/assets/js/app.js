function changeSearch(menuVal){
 if(menuVal == 'menuSearch1'){
 	$('#menuSearch1').attr('class', 'menu_active');
 	$('#menuSearch2').attr('class', 'menu');
 }
 else if(menuVal == 'menuSearch2'){
 	$('#menuSearch2').attr('class', 'menu_active');
 	$('#menuSearch1').attr('class', 'menu');
 }
}

function saveItem(sysid){
	ml = $('#'+sysid+'_ml').val();
	sysid = $('#'+sysid+'_sysid').val();
	city = $('#'+sysid+'_city').val();
	county = $('#'+sysid+'_county').val();
	street = $('#'+sysid+'_street').val();
	number = $('#'+sysid+'_number').val();
	zipcode = $('#'+sysid+'_zipcode').val();
	description = $('#'+sysid+'_description').val();
	bed = $('#'+sysid+'_bed').val();
	bathroom = $('#'+sysid+'_bathroom').val();
	area = $('#'+sysid+'_area').val();
	pool = $('#'+sysid+'_pool').val();
	price = $('#'+sysid+'_price').val();

	$.post( siteurl + "ajax_dashboard/saveItem", {
		ml: ml,
		sysid: sysid,
		city: city,
		county: county,
		street: street,
		number: number,
		zipcode: zipcode,
		description: description,
		bed: bed,
		bathroom: bathroom,
		area: area,
		pool: pool,
		price: price
	}).done(function(data){
		if(data == 1){
			alert('Imóvel salvo com sucesso!');
		} else {
			alert('Erro ao tentar salvar imóvel!');
		}
	})
}