<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function getSubDomain(){
  $items = (explode(".",$_SERVER['HTTP_HOST']));
  if(count($items) > 3){ //2 local, 3 web
    return $items[0];
  } else {
    return null;
  }
}


function PUBLIC_FOLDER_ACCESS(){
  echo 'http://' . $_SERVER['HTTP_HOST'] . '/public/';
}

function PUBLIC_FOLDER_ACCESS_OUT(){
 echo 'http://' . $_SERVER['HTTP_HOST'] . '/public/outside/'; 
}

function url_to($to){
  echo 'http://' . $_SERVER['HTTP_HOST'] . '/' . $to;
}

function siteurl(){
  echo 'http://' . $_SERVER['HTTP_HOST'] . '/';
}

function domain(){
  return 'http://' . $_SERVER['HTTP_HOST'];
}

function SITE_TITLE(){
  echo 'Miami Elite';
}

/*
*
* package: helper.structure
* date: 21-08-2013
* by: Fernando
*
*/
function sessionID(){
  $CI =& get_instance();
  return $CI->session->userdata('token');
}

function sessionNivel(){
  $CI =& get_instance();
  return $CI->session->userdata('nivel');
}


/**
* POST()
* Recebe um post via form
* @return void
*/
function POST()
{
  $CI =& get_instance();
  return $CI->input->post();
}

/**
* VIEW()
* Chama view, modo simplificado
* @param @view, $param
* @return void
*/
function VIEW($view, $param = null)
{
  $CI =& get_instance();
  if($param != null)
  {
    $CI->load->view($view, $param); 
  } else {
    $CI->load->view($view);
  }
}

/**
* MODEL()
* Chama view, modo simplificado
* @param @view, $param
* @return void
*/
function MODEL($model)
{
  $CI =& get_instance();

   $CI->load->model($model.'_model');
   return $CI;
}

/**
* redirect()
* Redireciona para outro lugar
* @param string $to
*/
function redirect($to = null)
{ 
  header('location: ' . BASEURL . $to);
}


/**
* ext()
* Extrai valor do array
* @param array data
* @param string index
* @return mixed value
*/
function getValue($data = null, $index = null)
{
	$value = '';
	if(!is_null($data) && !is_null($index) && is_array($data))
	{
		// passa os indices do array encaminhado como param para lower case
		$arr_aux = array_change_key_case($data, CASE_LOWER);
		$value = (isset($arr_aux[strtolower($index)])) ? $arr_aux[strtolower($index)] : '';
	}
	return $value;
}


/**
 * get_value()
 * Retorna um valor de um array, idependentemente do INDEX existir ou não.
 * @param array data
 * @param string index
 * @return mixed value
 */
function get_value($data = null, $index = null)
{
    $value = '';
    if(!is_null($data) && !is_null($index) && is_array($data))
    {
        // passa os indices do array encaminhado como param para lower case
        $arr_aux = array_change_key_case($data, CASE_LOWER);
        $value = (isset($arr_aux[strtolower($index)])) ? $arr_aux[strtolower($index)] : '';
    }
    return $value;
}


function getCity($city, $autocomplete = false){
  $districtMap = array(
    'North Miami' => 'NMIAMI',
    'Miami' => 'MIAMI',
    'North Miami Beach' => 'NMIAMIBC',
    'Fort Lauderdale' => 'FORTLAUD',
    'Pembroke Pines' => 'PEMBPINE',
    'Coral Gables' => 'CORALGBL',
    'Miami Lakes' => 'MIAMILKE',
    'Hollywood' => 'HOLLYWD',
    'Miramar' => 'MIRAMAR',
    'Pompano Beach' => 'POMPANO',
    'Pinecrest' => 'PINECRST',
    'Miami Shores' => 'MIAMISHR',
    'Lauderhill' => 'LAUDHILL',
    'Miami Gardens' => 'MIAMIGAR',
    'Weston' => 'WESTON',
    'Opa-locka' => 'OPALOCKA',
    'Homestead' => 'HOMESTED',
    'Homestead' => 'DADECNTY',
    'Miami Beach' => 'MIAMIBCH',
    'OTHERCTY' => 'OTHERCTY',
    'Plantation' => 'PLANTATN',
    'Key Biscayne' => 'KEYBISCY',
    'Dania Beach' => 'DANIABCH',
    'OTHERISL' => 'OTHERISL',
    'Tamarac' => 'TAMARAC',
    'ROYAL PB' => 'ROYALPB',
    'Cooper City' => 'COOPERCI',
    'Palm Bay' => 'PALMEBAY',
    'Hallandale Beach' => 'HALLNDLE',
    'Davie' => 'DAVIE',
    'Boynton Beach' => 'BOYNTON',
    'Hialeah' => 'HIALEAH',
    'North Lauderdale' => 'NLAUDER',
    'OTHERFLA' => 'OTHERFLA',
    'Surfside' => 'SURFSIDE',
    'Doral' => 'DORAL',
    'Fort Pierce' => 'FTPIERCE',
    'Miami Springs' => 'MIAMISPR',
    'Coral Springs' => 'CORALSPR',
    'Coconut Grove' => 'COCOGROV',
    'Cutler Bay' => 'CUTLRBAY',
    'Parkland' => 'PARKLAND',
    'Boca Raton' => 'BOCA',
    'Wellington' => 'WELLINGT',
    'Sunny Isles Beach' => 'SUNNYISL',
    'Aventura' => 'AVENTUR',
    'Palm Springs' => 'PALMSPRG',
    'ISLCARBN' => 'ISLCARBN',
    'DELRAY' => 'DELRAY',
    'Coconut Creek' => 'COCOCRK',
    'South Miami' => 'SMIAMI',
    'Hialeah Gardens' => 'HIALGRDN',
    'Margate' => 'MARGATE',
    'West Park' => 'WSTPRK',
    'OTHER' => 'OTHER',
    'Sunrise' => 'SUNRISE',
    'Loxahatchee' => 'LOXAHAT',
    'Naranja' => 'NARANJA',
    'Golden Beach' => 'GOLDNBCH',
    'Pembroke Park' => 'PEMBPARK',
    'Kendall' => 'KENDALL',
    'Lake Worth' => 'LAKEWORT',
    'Deerfield Beach' => 'DEERFLD',
    'Biscayne Park' => 'BISCPARK',
    'Lauderdale-By-The-Sea' => 'LAUDBSEA',
    'Southwest Ranches' => 'SWRANCH',
    'Port St Lucie' => 'PTSTLUCE',
    'Bay Harbor Islands' => 'BAYHARIS',
    'Oakland Park' => 'OAKPARK',
    'Riviera Beach' => 'RIVIERA',
    'Lauderdale Lakes' => 'LAUDLAKE',
    'Florida City' => 'FLACITY',
    'VIRGRDNS' => 'VIRGRDNS',
    'SWEETWTR' => 'SWEETWTR',
    'Wilton Manors' => 'WILTONMN',
    'El Portal' => 'ELPORTAL',
    'OTHERUSA' => 'OTHERUSA',
    'Lighthouse Point' => 'LHPOINT',
    'BISGRDNS' => 'BISGRDNS',
    'Lantana' => 'LANTANA',
    'Highland' => 'HIGHLAND',
    'Palm Beach Gardens' => 'PBGARDEN',
    'North Bay Village' => 'NBAYVLGE',
    'FISHISLD' => 'FISHISLD',
    'BALHARBR' => 'BALHARBR',
    'Greenacres' => 'GREENACR',
    'Palm Beach' => 'PALMBEAC',
    'EASTERNS' => 'EASTERNS',
    'Hillsboro Beach' => 'HILLSBRO',
    'Jupiter' => 'JUPITER',
    'HUTCHNSN' => 'HUTCHNSN',
    'BELLEGLA' => 'Belle Glade',
  );

    foreach($districtMap as $key => $value){
      if($city == $value){
        return $key;
      }
    }
    return $city;
}