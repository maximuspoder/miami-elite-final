<!DOCTYPE html>
<html>
<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>Miami Elite</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Font CSS (Via CDN) -->
<link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800'>
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300">
<!-- Dropzone CSS -->
<link rel="stylesheet" href="<?php PUBLIC_FOLDER_ACCESS(); ?>vendor/plugins/dropzone/downloads/css/dropzone.css">
<!-- Theme CSS -->
<link rel="stylesheet" type="text/css" href="<?php PUBLIC_FOLDER_ACCESS(); ?>assets/skin/default_skin/css/theme.css">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

</head>

<body class="form-uploaders-page">

 
    <div id="main">

       <header class="navbar navbar-fixed-top bg-light">
        <div class="navbar-branding">
            <a class="navbar-brand" href="dashboard.html"> <b>Miami</b>Elite </a>
            <span id="toggle_sidemenu_l" class="glyphicons glyphicons-show_lines"></span>
            <ul class="nav navbar-nav pull-right hidden">
                <li>
                    <a href="#" class="sidebar-menu-toggle">
                        <span class="octicon octicon-ruby fs20 mr10 pull-right "></span>
                    </a>
                </li>
            </ul>
        </div>

    </header>

    <?php template_admin_navigation($this->session->userdata('tipousuario')); ?>

    <section id="content_wrapper">
        <section id="content" class="pn">
            <div class="tray tray-center pn">
                <form   action="<?php siteurl(); ?>dashboard/featured_upload_process/<?php echo $uploadid; ?>" 
                        class="dropzone" 
                        id="dropZone23">
                    <div class="fallback">
                        <input name="file" type="file" multiple />
                    </div>
                </form>
            </div>
        </section>
    </section>









    </div>
    <!-- End: Main -->

    <script type="text/javascript" src="<?php PUBLIC_FOLDER_ACCESS(); ?>vendor/jquery/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="<?php PUBLIC_FOLDER_ACCESS(); ?>vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php PUBLIC_FOLDER_ACCESS(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php PUBLIC_FOLDER_ACCESS(); ?>vendor/plugins/dropzone/downloads/dropzone.min.js"></script>
    <script type="text/javascript" src="<?php PUBLIC_FOLDER_ACCESS(); ?>assets/js/utility/utility.js"></script>
    <script type="text/javascript" src="<?php PUBLIC_FOLDER_ACCESS(); ?>assets/js/main.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            "use strict";
            Core.init();

            Dropzone.autoDiscover = false;
            $(".dropzone").dropzone({
                dictDefaultMessage: '<i class="fa fa-cloud-upload"></i> \
                <span class="main-text"><b>Arraste a foto</b> para enviar</span> <br /> \
                <span class="sub-text">(ou clique)</span> \
                ',
                maxFilesize: 2, // MB
                init: function() {
                    this.on("success", function(file, responseText) {
                        console.log(responseText);
                        //$('.dz-success-mark').hide();
                        //$('.dz-error-mark').show();
                    });
                }
            });
        });
    </script>
    <!-- END: PAGE SCRIPTS -->

</body>

</html>