<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title><?php SITE_TITLE(); ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800'>
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300">
	<link rel="stylesheet" type="text/css" href="<?php PUBLIC_FOLDER_ACCESS(); ?>assets/skin/default_skin/css/theme.css">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
	<script>siteurl = '<?php siteurl() ?>'</script>

</head>
<body class="blank-page">
<div id="main">

	<header class="navbar navbar-fixed-top bg-light">
		<div class="navbar-branding">
			<a class="navbar-brand" href="<?php siteurl(); ?>dashboard/"> <b>Miami</b>Elite </a>
			<span id="toggle_sidemenu_l" class="glyphicons glyphicons-show_lines"></span>
			<ul class="nav navbar-nav pull-right hidden">
				<li>
					<a href="#" class="sidebar-menu-toggle">
						<span class="octicon octicon-ruby fs20 mr10 pull-right "></span>
					</a>
				</li>
			</ul>
		</div>
	</header>

	<?php template_admin_navigation($this->session->userdata('tipousuario')); ?>

	<!-- Start: Content -->
	<section id="content_wrapper">

		<header id="topbar">
            <div class="topbar-left">
                <ol class="breadcrumb">
                    <li class="crumb-active">
                        <a href="<?php siteurl(); ?>dashboard/">Pesquisa</a>
                    </li>
                    <?php if($params->mlnumber != ''){ ?>
                    <li class="crumb-trail">ML Number <strong><?php echo $params->mlnumber; ?></strong></li>
                    <?php } ?>
                    <?php if($params->zipcode != ''){ ?>
                    <li class="crumb-trail">Zip Code <strong><?php echo $params->zipcode; ?></strong></li>
                    <?php } ?>
                    <?php if($cities != "") { ?>
                    <li class="crumb-trail">Cities <strong><?php echo $cities; ?></strong></li>
                    <?php } ?>
                </ol>
            </div>
        </header>


		<section id="content">

			<?php if($params->quantity == 'null' && $params->zipcode == '' && $params->mlnumber == ''){ ?>
			<div class="alert alert-info pastel alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <i class="fa fa-info pr10"></i>
                <strong>Ajuda?</strong> Utilize o filtro para obter um melhor resultado em suas pesquisas.
            </div>
            <?php } ?>


			<div class="col-md-12">
				<?php if(count($property1) > 0){ ?>
					<?php foreach($property1 as $result): ?>
						<div class="panel">
							<div class="panel-heading">
								<span class="panel-title">ML Number: <?php echo @$result->field_157; ?></span>
							</div>
							<div class="panel-body">
								<div class="col-md-4">
									<?php
						                if(file_exists(PHOTOS .'photos_'.@$result->sysid.'/1.jpg')){
						                    $photo1 = UPLOADLINK . 'photos_'.@$result->sysid.'/1.jpg';
						            ?>
						            	<div class="mix label3 folder3" style="display: inline-block;">
						                    <div class="panel p6 pbn">
						                        <div class="of-h">
						                            <img src="<?php echo $photo1; ?>" class="h-170" title="gallery rets image">
						                        </div>
						                    </div>
						                </div>
						            <?php
						                } else {
						                	$photo1 = UPLOADLINK . 'default.jpg';
						            ?>
						            	<div class="mix label3 folder3" style="display: inline-block;">
						                    <div class="panel p6 pbn">
						                        <div class="of-h">
						                            <img src="<?php echo $photo1; ?>" class="h-170" title="gallery rets image">
						                        </div>
						                    </div>
						                </div>
						            <?php
						                }
						            ?>
								</div>
								<div class="col-md-4">
									Sysid: <strong><?php echo @$result->sysid; ?></strong><br/>
									ML Number: <strong><?php echo @$result->field_157; ?></strong><br/>
									Cidade: 
									<strong>
									<?php 
										foreach ($districtMap as $key => $value){
											if( $result->field_922  == $value ){
												echo $key;
											}
										}
									?>
									</strong><br/>
								</div>
								<div class="col-md-4">
									Zip Code: <strong><?php echo @$result->field_10; ?></strong><br/>
									Descrição: <strong><?php echo @$result->field_1339; ?></strong><br/>
									Preço: <strong><?php echo @$result->field_137; ?></strong><br/>
								</div>
								<div class="col-md-12">
									<br>
									<button class="btn btn-default" onclick="redirectTo('<?php echo siteurl() .'dashboard/showProperty/1/'. $result->sysid; ?>')">Vizualizar</button>
									ou
									<button id="btn_<?php echo @$result->field_157; ?>" class="btn btn-default" 
									onclick="save('<?php echo @$result->field_157; ?>', '<?php echo $this->session->userdata('id'); ?>')">
									Salvar
									</button>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				<?php } ?>

				<?php if(count($property2) > 0){ ?>
					<?php foreach($property2 as $result): ?>
						<div class="panel">
							<div class="panel-heading">
								<span class="panel-title">ML Number: <?php echo @$result->field_157; ?></span>
							</div>
							<div class="panel-body">
								<div class="col-md-4">
									<?php
						                if(file_exists(PHOTOS .'photos_'.@$result->sysid.'/1.jpg')){
						                    $photo1 = UPLOADLINK . 'photos_'.@$result->sysid.'/1.jpg';
						            ?>
						            	<div class="mix label3 folder3" style="display: inline-block;">
						                    <div class="panel p6 pbn">
						                        <div class="of-h">
						                            <img src="<?php echo $photo1; ?>" class="h-170" title="gallery rets image">
						                        </div>
						                    </div>
						                </div>
						            <?php
						                } else {
						                	$photo1 = UPLOADLINK . 'default.jpg';
						            ?>
						            	<div class="mix label3 folder3" style="display: inline-block;">
						                    <div class="panel p6 pbn">
						                        <div class="of-h">
						                            <img src="<?php echo $photo1; ?>" class="h-170" title="gallery rets image">
						                        </div>
						                    </div>
						                </div>
						            <?php
						                }
						            ?>
								</div>
								<div class="col-md-4">
									Sysid: <strong><?php echo @$result->sysid; ?></strong><br/>
									ML Number: <strong><?php echo @$result->field_157; ?></strong><br/>
									Cidade:
									<strong>
									<?php 
										foreach ($districtMap as $key => $value){
											if( $result->field_922  == $value ){
												echo $key;
											}
										}
									?>
									</strong><br/>
								</div>
								<div class="col-md-4">
									Zip Code: <strong><?php echo @$result->field_10; ?></strong><br/>
									Descrição: <strong><?php echo @$result->field_1339; ?></strong><br/>
									Preço: <strong><?php echo @$result->field_137; ?></strong><br/>
								</div>
								<div class="col-md-12">
									<br>
									<button class="btn btn-default" onclick="redirectTo('<?php echo siteurl() .'dashboard/showProperty/2/'. $result->sysid; ?>')">Vizualizar</button>
									ou
									<button class="btn btn-default" 
									onclick="save('<?php echo @$result->field_157; ?>', '<?php echo $this->session->userdata('id'); ?>')">
									Salvar
									</button>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				<?php } ?>

				<?php if(count($property3) > 0){ ?>
					<?php foreach($property3 as $result): ?>
					<div class="panel">
						<div class="panel-heading">
							<span class="panel-title">ML Number: <?php echo $result->field_157; ?></span>
						</div>
						<div class="panel-body">
							<div class="col-md-4">
								<?php
					                if(file_exists(PHOTOS .'photos_'.@$result->sysid.'/1.jpg')){
					                    $photo1 = UPLOADLINK . 'photos_'.@$result->sysid.'/1.jpg';
					            ?>
					            	<div class="mix label3 folder3" style="display: inline-block;">
					                    <div class="panel p6 pbn">
					                        <div class="of-h">
					                            <img src="<?php echo $photo1; ?>" class="h-170" title="gallery rets image">
					                        </div>
					                    </div>
					                </div>
					            <?php
					                } else {
					                	$photo1 = UPLOADLINK . 'default.jpg';
					            ?>
					            	<div class="mix label3 folder3" style="display: inline-block;">
					                    <div class="panel p6 pbn">
					                        <div class="of-h">
					                            <img src="<?php echo $photo1; ?>" class="h-170" title="gallery rets image">
					                        </div>
					                    </div>
					                </div>
					            <?php
					                }
					            ?>
							</div>
							<div class="col-md-4">
								Sysid: <strong><?php echo @$result->sysid; ?></strong><br/>
								ML Number: <strong><?php echo @$result->field_157; ?></strong><br/>
								Cidade:
								<strong>
									<?php 
										foreach ($districtMap as $key => $value){
											if( $result->field_922  == $value ){
												echo $key;
											}
										}
									?>
								</strong><br/>
							</div>
							<div class="col-md-4">
								Zip Code: <strong><?php echo @$result->field_10; ?></strong><br/>
								Descrição: <strong><?php echo @$result->field_1339; ?></strong><br/>
								Preço: <strong><?php echo @$result->field_137; ?></strong><br/>
							</div>
							<div class="col-md-12">
								<br>
								<button class="btn btn-default" onclick="redirectTo('<?php echo siteurl() .'dashboard/showProperty/3/'. $result->sysid; ?>')">Vizualizar</button>
								ou
								<button class="btn btn-default" 
									onclick="save('<?php echo @$result->field_157; ?>', '<?php echo $this->session->userdata('id'); ?>')">
									Salvar
									</button>
							</div>
						</div>
					</div>
					<?php endforeach; ?>
				<?php } ?>

				<?php if(count($property4) > 0){ ?>
					<?php foreach($property4 as $result): ?>
						<div class="panel">
							<div class="panel-heading">
								<span class="panel-title">ML Number: <?php echo $result->field_157; ?></span>
							</div>
							<div class="panel-body">
								<div class="col-md-4">
									<?php
						                if(file_exists(PHOTOS .'photos_'.@$result->sysid.'/1.jpg')){
						                    $photo1 = UPLOADLINK . 'photos_'.@$result->sysid.'/1.jpg';
						            ?>
						            	<div class="mix label3 folder3" style="display: inline-block;">
						                    <div class="panel p6 pbn">
						                        <div class="of-h">
						                            <img src="<?php echo $photo1; ?>" class="h-170" title="gallery rets image">
						                        </div>
						                    </div>
						                </div>
						            <?php
						                } else {
						                	$photo1 = UPLOADLINK . 'default.jpg';
						            ?>
						            	<div class="mix label3 folder3" style="display: inline-block;">
						                    <div class="panel p6 pbn">
						                        <div class="of-h">
						                            <img src="<?php echo $photo1; ?>" class="h-170" title="gallery rets image">
						                        </div>
						                    </div>
						                </div>
						            <?php
						                }
						            ?>
								</div>
								<div class="col-md-4">
									Sysid: <strong><?php echo @$result->sysid; ?></strong><br/>
									ML Number: <strong><?php echo @$result->field_157; ?></strong><br/>
									Cidade:
									<strong>
									<?php 
										foreach ($districtMap as $key => $value){
											if( $result->field_922  == $value ){
												echo $key;
											}
										}
									?>
									</strong><br/>
								</div>
								<div class="col-md-4">
									Zip Code: <strong><?php echo @$result->field_10; ?></strong><br/>
									Descrição: <strong><?php echo @$result->field_1339; ?></strong><br/>
									Preço: <strong><?php echo @$result->field_137; ?></strong><br/>
								</div>
								<div class="col-md-12">
									<br>
									<button class="btn btn-default" onclick="redirectTo('<?php echo siteurl() .'dashboard/showProperty/4/'. $result->sysid; ?>')">Vizualizar</button>
									ou
									<button class="btn btn-default" 
									onclick="save('<?php echo @$result->field_157; ?>', '<?php echo $this->session->userdata('id'); ?>')">
									Salvar
									</button>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				<?php } ?>

				<?php if(count($property5) > 0){ ?>
					<?php foreach($property5 as $result): ?>
						<div class="panel">
							<div class="panel-heading">
								<span class="panel-title">ML Number: <?php echo $result->field_157; ?></span>
							</div>
							<div class="panel-body">
								<div class="col-md-4">
									<?php
						                if(file_exists(PHOTOS .'photos_'.@$result->sysid.'/1.jpg')){
						                    $photo1 = UPLOADLINK . 'photos_'.@$result->sysid.'/1.jpg';
						            ?>
						            	<div class="mix label3 folder3" style="display: inline-block;">
						                    <div class="panel p6 pbn">
						                        <div class="of-h">
						                            <img src="<?php echo $photo1; ?>" class="h-170" title="gallery rets image">
						                        </div>
						                    </div>
						                </div>
						            <?php
						                } else {
						                	$photo1 = UPLOADLINK . 'default.jpg';
						            ?>
						            	<div class="mix label3 folder3" style="display: inline-block;">
						                    <div class="panel p6 pbn">
						                        <div class="of-h">
						                            <img src="<?php echo $photo1; ?>" class="h-170" title="gallery rets image">
						                        </div>
						                    </div>
						                </div>
						            <?php
						                }
						            ?>
								</div>
								<div class="col-md-4">
									Sysid: <strong><?php echo @$result->sysid; ?></strong><br/>
									ML Number: <strong><?php echo @$result->field_157; ?></strong><br/>
									Cidade:
									<strong>
									<?php 
										foreach ($districtMap as $key => $value){
											if( $result->field_922  == $value ){
												echo $key;
											}
										}
									?>
									</strong><br/>
								</div>
								<div class="col-md-4">
									Zip Code: <strong><?php echo @$result->field_10; ?></strong><br/>
									Descrição: <strong><?php echo @$result->field_1339; ?></strong><br/>
									Preço: <strong><?php echo @$result->field_137; ?></strong><br/>
								</div>
								<div class="col-md-12">
									<br>
									<button class="btn btn-default" onclick="redirectTo('<?php echo siteurl() .'dashboard/showProperty/5/'. $result->sysid; ?>')">Vizualizar</button>
									ou
									<button class="btn btn-default" 
									onclick="save('<?php echo @$result->field_157; ?>', '<?php echo $this->session->userdata('id'); ?>')">
									Salvar
									</button>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				<?php } ?>

				<?php if(count($property6) > 0){ ?>
					<?php foreach($property6 as $result): ?>
						<div class="panel">
							<div class="panel-heading">
								<span class="panel-title">ML Number: <?php echo $result->field_157; ?></span>
							</div>
							<div class="panel-body">
								<div class="col-md-4">
									<?php
						                if(file_exists(PHOTOS .'photos_'.@$result->sysid.'/1.jpg')){
						                    $photo1 = UPLOADLINK . 'photos_'.@$result->sysid.'/1.jpg';
						            ?>
						            	<div class="mix label3 folder3" style="display: inline-block;">
						                    <div class="panel p6 pbn">
						                        <div class="of-h">
						                            <img src="<?php echo $photo1; ?>" class="h-170" title="gallery rets image">
						                        </div>
						                    </div>
						                </div>
						            <?php
						                } else {
						                	$photo1 = UPLOADLINK . 'default.jpg';
						            ?>
						            	<div class="mix label3 folder3" style="display: inline-block;">
						                    <div class="panel p6 pbn">
						                        <div class="of-h">
						                            <img src="<?php echo $photo1; ?>" class="h-170" title="gallery rets image">
						                        </div>
						                    </div>
						                </div>
						            <?php
						                }
						            ?>
								</div>
								<div class="col-md-4">
									Sysid: <strong><?php echo @$result->sysid; ?></strong><br/>
									ML Number: <strong><?php echo @$result->field_157; ?></strong><br/>
									Cidade:
									<strong>
									<?php 
										foreach ($districtMap as $key => $value){
											if( $result->field_922  == $value ){
												echo $key;
											}
										}
									?>
									</strong><br/>
								</div>
								<div class="col-md-4">
									Zip Code: <strong><?php echo @$result->field_10; ?></strong><br/>
									Descrição: <strong><?php echo @$result->field_1339; ?></strong><br/>
									Preço: <strong><?php echo @$result->field_137; ?></strong><br/>
								</div>
								<div class="col-md-12">
									<br>
									<button class="btn btn-default" onclick="redirectTo('<?php echo siteurl() .'dashboard/showProperty/6/'. $result->sysid; ?>')">Vizualizar</button>
									ou
									<button class="btn btn-default" 
									onclick="save('<?php echo @$result->field_157; ?>', '<?php echo $this->session->userdata('id'); ?>')">
									Salvar
									</button>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				<?php } ?>

				<?php if(count($property7) > 0){ ?>
					<?php foreach($property7 as $result): ?>
						<div class="panel">
							<div class="panel-heading">
								<span class="panel-title">ML Number: <?php echo $result->field_157; ?></span>
							</div>
							<div class="panel-body">
								<div class="col-md-4">
									<?php
						                if(file_exists(PHOTOS .'photos_'.@$result->sysid.'/1.jpg')){
						                    $photo1 = UPLOADLINK . 'photos_'.@$result->sysid.'/1.jpg';
						            ?>
						            	<div class="mix label3 folder3" style="display: inline-block;">
						                    <div class="panel p6 pbn">
						                        <div class="of-h">
						                            <img src="<?php echo $photo1; ?>" class="h-170" title="gallery rets image">
						                        </div>
						                    </div>
						                </div>
						            <?php
						                } else {
						                	$photo1 = UPLOADLINK . 'default.jpg';
						            ?>
						            	<div class="mix label3 folder3" style="display: inline-block;">
						                    <div class="panel p6 pbn">
						                        <div class="of-h">
						                            <img src="<?php echo $photo1; ?>" class="h-170" title="gallery rets image">
						                        </div>
						                    </div>
						                </div>
						            <?php
						                }
						            ?>
								</div>
								<div class="col-md-4">
									Sysid: <strong><?php echo @$result->sysid; ?></strong><br/>
									ML Number: <strong><?php echo @$result->field_157; ?></strong><br/>
									Cidade:
									<strong>
									<?php 
										foreach ($districtMap as $key => $value){
											if( $result->field_922  == $value ){
												echo $key;
											}
										}
									?>
									</strong><br/>
								</div>
								<div class="col-md-4">
									Zip Code: <strong><?php echo @$result->field_10; ?></strong><br/>
									Descrição: <strong><?php echo @$result->field_1339; ?></strong><br/>
									Preço: <strong><?php echo @$result->field_137; ?></strong><br/>
								</div>
								<div class="col-md-12">
									<br>
									<button class="btn btn-default" onclick="redirectTo('<?php echo siteurl() .'dashboard/showProperty/7/'. $result->sysid; ?>')">Vizualizar</button>
									ou
									<button class="btn btn-default" 
									onclick="save('<?php echo @$result->field_157; ?>', '<?php echo $this->session->userdata('id'); ?>')">
									Salvar
									</button>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				<?php } ?>

				<?php if(count($property8) > 0){ ?>
					<?php foreach($property8 as $result): ?>
						<div class="panel">
							<div class="panel-heading">
								<span class="panel-title">ML Number: <?php echo $result->field_157; ?></span>
							</div>
							<div class="panel-body">
								<div class="col-md-4">
									<?php
						                if(file_exists(PHOTOS .'photos_'.@$result->sysid.'/1.jpg')){
						                    $photo1 = UPLOADLINK . 'photos_'.@$result->sysid.'/1.jpg';
						            ?>
						            	<div class="mix label3 folder3" style="display: inline-block;">
						                    <div class="panel p6 pbn">
						                        <div class="of-h">
						                            <img src="<?php echo $photo1; ?>" class="h-170" title="gallery rets image">
						                        </div>
						                    </div>
						                </div>
						            <?php
						                } else {
						                	$photo1 = UPLOADLINK . 'default.jpg';
						            ?>
						            	<div class="mix label3 folder3" style="display: inline-block;">
						                    <div class="panel p6 pbn">
						                        <div class="of-h">
						                            <img src="<?php echo $photo1; ?>" class="h-160" title="gallery rets image">
						                        </div>
						                    </div>
						                </div>
						            <?php
						                }
						            ?>
								</div>
								<div class="col-md-4">
									Sysid: <strong><?php echo @$result->sysid; ?></strong><br/>
									ML Number: <strong><?php echo @$result->field_157; ?></strong><br/>
									Cidade:
									<strong>
									<?php 
										foreach ($districtMap as $key => $value){
											if( $result->field_922  == $value ){
												echo $key;
											}
										}
									?>
									</strong><br/>
								</div>
								<div class="col-md-4">
									Zip Code: <strong><?php echo @$result->field_10; ?></strong><br/>
									Descrição: <strong><?php echo @$result->field_1339; ?></strong><br/>
									Preço: <strong><?php echo @$result->field_137; ?></strong><br/>
								</div>
								<div class="col-md-12">
									<br>
									<button class="btn btn-default" onclick="redirectTo('<?php echo siteurl() .'dashboard/showProperty/8/'. $result->sysid; ?>')">Vizualizar</button>
									ou
									<button class="btn btn-default" 
									onclick="save('<?php echo @$result->field_157; ?>', '<?php echo $this->session->userdata('id'); ?>')">
									Salvar
									</button>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				<?php } ?>
			</div>

		</section>


	</section>
</div>
<!-- jQuery -->
<script type="text/javascript" src="<?php PUBLIC_FOLDER_ACCESS(); ?>vendor/jquery/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="<?php PUBLIC_FOLDER_ACCESS(); ?>vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="<?php PUBLIC_FOLDER_ACCESS(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
<!-- Theme Javascript -->
<script type="text/javascript" src="<?php PUBLIC_FOLDER_ACCESS(); ?>assets/js/utility/utility.js"></script>
<script type="text/javascript" src="<?php PUBLIC_FOLDER_ACCESS(); ?>assets/js/main.js"></script>
<script type="text/javascript" src="<?php PUBLIC_FOLDER_ACCESS(); ?>assets/js/admin.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function() {
		"use strict";
		Core.init();
	});
</script>
</body>
</html>