<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title><?php SITE_TITLE(); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800'>
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300">
    <link rel="stylesheet" type="text/css" href="<?php PUBLIC_FOLDER_ACCESS(); ?>assets/skin/default_skin/css/theme.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <script>siteurl = '<?php siteurl() ?>'</script>

</head>
<body class="blank-page">
<div id="main">

    <header class="navbar navbar-fixed-top bg-light">
        <div class="navbar-branding">
            <a class="navbar-brand" href="dashboard.html"> <b>Miami</b>Elite </a>
            <span id="toggle_sidemenu_l" class="glyphicons glyphicons-show_lines"></span>
            <ul class="nav navbar-nav pull-right hidden">
                <li>
                    <a href="#" class="sidebar-menu-toggle">
                        <span class="octicon octicon-ruby fs20 mr10 pull-right "></span>
                    </a>
                </li>
            </ul>
        </div>

    </header>

    <?php template_admin_navigation($this->session->userdata('tipousuario')); ?>

    <!-- Start: Content -->
    <section id="content_wrapper">
        <section id="content">
            <h2>Informações do imóvel. ML Number: <?php echo $result[0]['mlnumber']; ?></h2>

            <?php
                foreach($result[0] as $key => $data):
                    foreach($map  as $keyMap => $dataMap):
                        if($key == 'field_'.$keyMap && strlen($data) > 0){
                        ?>
                            <div class="col-md-10">
                                <?php echo $dataMap; ?>:
                                <strong><?php echo $data; ?> </strong><br/>
                            </div>
                        <?php
                        }
                    endforeach;
                endforeach;
            ?>
            <div style="clear:both"></div>
            <br/>

            <div id="mix-container">
                <?php
                if(file_exists(PHOTOS .'photos_'.$sysid.'/1.jpg')){
                    $photos[0] = UPLOADLINK . 'photos_'.$sysid.'/1.jpg';
                }
                if(file_exists(PHOTOS .'photos_'.$sysid.'/2.jpg')){
                    $photos[1] = UPLOADLINK . 'photos_'.$sysid.'/2.jpg';
                }
                if(file_exists(PHOTOS .'photos_'.$sysid.'/3.jpg')){
                    $photos[2] = UPLOADLINK . 'photos_'.$sysid.'/3.jpg';
                }
                if(file_exists(PHOTOS .'photos_'.$sysid.'/4.jpg')){
                    $photos[3] = UPLOADLINK . 'photos_'.$sysid.'/4.jpg';
                }
                if(file_exists(PHOTOS .'photos_'.$sysid.'/5.jpg')){
                    $photos[4] = UPLOADLINK . 'photos_'.$sysid.'/5.jpg';
                }
                if(file_exists(PHOTOS .'photos_'.$sysid.'/6.jpg')){
                    $photos[5] = UPLOADLINK . 'photos_'.$sysid.'/6.jpg';
                }
                if(file_exists(PHOTOS .'photos_'.$sysid.'/7.jpg')){
                    $photos[6] = UPLOADLINK . 'photos_'.$sysid.'/7.jpg';
                }
                if(file_exists(PHOTOS .'photos_'.$sysid.'/7.jpg')){
                    $photos[7] = UPLOADLINK . 'photos_'.$sysid.'/7.jpg';
                }


                ?>
                <?php $x = 1; foreach($photos as $photo): ?>
                <div class="col-md-3">
                <div class="mix label3 folder3" style="display: inline-block;">
                    <div class="panel p6 pbn">
                        <div class="of-h">
                            <img src="<?php echo $photo; ?>" class="h-170" title="gallery rets image">
                            <div class="row table-layout">
                                <div class="col-md-12 va-m pln">
                                    <h6>Photo <?php echo $x; ?></h6>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                </div>
                <?php $x++; endforeach; ?>
            </div>

        </section>


    </section>
</div>
<!-- jQuery -->
<script type="text/javascript" src="<?php PUBLIC_FOLDER_ACCESS(); ?>vendor/jquery/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="<?php PUBLIC_FOLDER_ACCESS(); ?>vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="<?php PUBLIC_FOLDER_ACCESS(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
<!-- Theme Javascript -->
<script type="text/javascript" src="<?php PUBLIC_FOLDER_ACCESS(); ?>assets/js/utility/utility.js"></script>
<script type="text/javascript" src="<?php PUBLIC_FOLDER_ACCESS(); ?>assets/js/main.js"></script>
<script type="text/javascript" src="<?php PUBLIC_FOLDER_ACCESS(); ?>assets/js/admin.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        "use strict";
        Core.init();
    });
</script>
</body>
</html>