<!DOCTYPE html>
<html>
<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>Miami Elite</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Font CSS (Via CDN) -->
<link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800'>
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300">
<!-- Dropzone CSS -->
<link rel="stylesheet" href="<?php PUBLIC_FOLDER_ACCESS(); ?>vendor/plugins/dropzone/downloads/css/dropzone.css">
<!-- Theme CSS -->
<link rel="stylesheet" type="text/css" href="<?php PUBLIC_FOLDER_ACCESS(); ?>assets/skin/default_skin/css/theme.css">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

</head>

<body class="form-uploaders-page">

 
    <div id="main">

       <header class="navbar navbar-fixed-top bg-light">
        <div class="navbar-branding">
            <a class="navbar-brand" href="javascript:void(0)"> <b>Miami</b>Elite </a>
            <span id="toggle_sidemenu_l" class="glyphicons glyphicons-show_lines"></span>
            <ul class="nav navbar-nav pull-right hidden">
                <li>
                    <a href="#" class="sidebar-menu-toggle">
                        <span class="octicon octicon-ruby fs20 mr10 pull-right "></span>
                    </a>
                </li>
            </ul>
        </div>

    </header>

    <?php template_admin_navigation($this->session->userdata('tipousuario')); ?>



    <section id="content_wrapper">        

        <header id="topbar">
            <div class="topbar-left">
                <ol class="breadcrumb">
                    <li class="crumb-active">
                        <a href="javascript:void(0)">Destaques</a>
                    </li>
                    <li class="crumb-link">
                        <b>Todos</b>
                    </li>
                    <li class="crumb-trail">
                        <a href="<?php echo url_to('dashboard/featured_new'); ?>">Clique Para Criar Novo</a>
                    </li>
                </ol>
            </div>
        </header>
        <br/><br/>



        <div class="col-md-12">
        <div class="panel panel-visible">
            <div class="panel-heading br-b-n">
                <div class="panel-title hidden-xs">
                    <span class="glyphicon glyphicon-tasks"></span>Destaques</div>
            </div>
            <div class="panel-body pn">
            <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer"><table class="table table-striped table-bordered table-hover dataTable no-footer" id="datatable" cellspacing="0" width="100%" role="grid" aria-describedby="datatable_info" style="width: 100%;">
                <thead>
                    <tr role="row">
                        <th class="sorting_asc" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 179px;">
                            Quartos</th>
                        <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 268px;">
                            Banheiros</th>
                        <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 96px;">
                            Descrição</th>
                        <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-label="Age: activate to sort column ascending" style="width: 63px;">
                            Data</th>
                        <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-label="Age: activate to sort column ascending" style="width: 63px;">
                        Deletar
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($featured as $result){ ?>
                    <tr role="row" class="odd">
                        <td><?php echo $result->quartos; ?></td>
                        <td><?php echo $result->banheiros; ?></td>
                        <td><?php echo substr($result->descricao, 0, 100).'...'; ?></td>
                        <td><?php echo $result->created_at; ?></td>
                        <td><a href="<?php echo url_to('dashboard/featured_delete/'.$result->featuredid ); ?>" class="btn btn-default">Deletar</a></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            </div>
        </div>
        </div>

    </section>


    </div>
    <!-- End: Main -->

    <script type="text/javascript" src="<?php PUBLIC_FOLDER_ACCESS(); ?>vendor/jquery/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="<?php PUBLIC_FOLDER_ACCESS(); ?>vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php PUBLIC_FOLDER_ACCESS(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php PUBLIC_FOLDER_ACCESS(); ?>assets/js/utility/utility.js"></script>
    <script type="text/javascript" src="<?php PUBLIC_FOLDER_ACCESS(); ?>assets/js/main.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            "use strict";
            Core.init();
        });
    </script>
    <!-- END: PAGE SCRIPTS -->

</body>

</html>