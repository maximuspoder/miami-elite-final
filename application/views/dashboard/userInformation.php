<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title><?php SITE_TITLE(); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800'>
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300">
    <link rel="stylesheet" href="<?php PUBLIC_FOLDER_ACCESS(); ?>vendor/plugins/dropzone/downloads/css/dropzone.css">
    <link rel="stylesheet" type="text/css" href="<?php PUBLIC_FOLDER_ACCESS(); ?>assets/skin/default_skin/css/theme.css">
    <!-- Dropzone CSS -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <script>siteurl = '<?php siteurl() ?>'</script>

</head>
<body class="blank-page">
<div id="main">

    <header class="navbar navbar-fixed-top bg-light">
        <div class="navbar-branding">
            <a class="navbar-brand" href="<?php siteurl(); ?>dashboard/"> <b>Miami</b>Elite </a>
            <span id="toggle_sidemenu_l" class="glyphicons glyphicons-show_lines"></span>
            <ul class="nav navbar-nav pull-right hidden">
                <li>
                    <a href="#" class="sidebar-menu-toggle">
                        <span class="octicon octicon-ruby fs20 mr10 pull-right "></span>
                    </a>
                </li>
            </ul>
        </div>
    </header>

    <?php template_admin_navigation($this->session->userdata('tipousuario')); ?>

    <!-- Start: Content -->
    <section id="content_wrapper">
        <section id="content" class="pn">
            <div class="tray tray-center pn">
                <form   action="<?php siteurl(); ?>dashboard/userInformation_upload/<?php echo $user[0]->id; ?>" 
                        class="dropzone" 
                        id="dropZone23">
                    <div class="fallback">
                        <input name="file" type="file" multiple />
                    </div>
                </form>
            </div>
        </section>
        <br/>


        <section id="content">
            <div class="col-md-12">
                <?php if($updated == 1){ ?>
                <div class="" data-animate="[&quot;100&quot;,&quot;fadeIn&quot;]">
                    <h2 class="lh30 mt15 text-center"><?php echo $info; ?></h2>
                    <p class="lead mb30 text-center center-block mw800"> <?php echo $message; ?></p>
                </div>
                <?php } ?>

                <div class="panel">
                    <div class="panel-heading">
                        <span class="panel-title">Meus Dados</span>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" action="<?php siteurl(); ?>dashboard/userInformation_process" method="post" role="form">
                            <div class="form-group">
                                <label for="inputStandard" class="col-lg-3 control-label">Domínio</label>
                                <div class="col-lg-8">
                                    <input type="text" value="<?php echo $user[0]->subdominio; ?>" disabled id="inputStandard" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputStandard" class="col-lg-3 control-label">Nome</label>
                                <div class="col-lg-8">
                                    <input type="text" name="novo_nome" value="<?php echo $user[0]->nome_completo; ?>" id="inputStandard" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputStandard" class="col-lg-3 control-label">Email</label>
                                <div class="col-lg-8">
                                    <input type="hidden" name="email" value="<?php echo $user[0]->email; ?>" style="display:none">
                                    <input type="password" name="senha" value="<?php echo $user[0]->email; ?>"  style="display:none">
                                    <input type="text" name="novo_email"value="<?php echo $user[0]->email; ?>" id="inputStandard" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputStandard" class="col-lg-3 control-label">Senha</label>
                                <div class="col-lg-8">
                                    <input type="password" name="nova_senha" value="" id="inputStandard" class="form-control" placeholder="Para alterar digite uma nova senha">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputStandard" class="col-lg-3 control-label">Creci</label>
                                <div class="col-lg-8">
                                    <input type="text" name="novo_creci"value="<?php echo $user[0]->creci; ?>" id="inputStandard" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputStandard" class="col-lg-3 control-label">Telefone</label>
                                <div class="col-lg-8">
                                    <input type="text" name="novo_telefone"value="<?php echo $user[0]->telefone; ?>" id="inputStandard" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputStandard" class="col-lg-3 control-label">Foto</label>
                                <div class="col-lg-8">
                                    <input type="text" value="<?php echo $user[0]->profile_picture; ?>" id="inputStandard" class="form-control"disabled>
                                </div>
                            </div>
                    
                            <div class="form-group">
                                <label for="inputStandard" class="col-lg-3 control-label"></label>
                                <div class="col-lg-8">
                                    <input type="submit" value="Alterar" class="btn btn-primary">
                                </div>
                            </div>
                        </form>


                         
                    </div>
                </div>
            </div>
        </section>
    </section>
</div>
<!-- jQuery -->
<script type="text/javascript" src="<?php PUBLIC_FOLDER_ACCESS(); ?>vendor/jquery/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="<?php PUBLIC_FOLDER_ACCESS(); ?>vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="<?php PUBLIC_FOLDER_ACCESS(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php PUBLIC_FOLDER_ACCESS(); ?>vendor/plugins/dropzone/downloads/dropzone.min.js"></script>
<!-- Theme Javascript -->
<script type="text/javascript" src="<?php PUBLIC_FOLDER_ACCESS(); ?>assets/js/utility/utility.js"></script>
<script type="text/javascript" src="<?php PUBLIC_FOLDER_ACCESS(); ?>assets/js/main.js"></script>
<script type="text/javascript" src="<?php PUBLIC_FOLDER_ACCESS(); ?>assets/js/admin.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        "use strict";
        Core.init();
        Dropzone.autoDiscover = false;
            $(".dropzone").dropzone({
                dictDefaultMessage: '<i class="fa fa-cloud-upload"></i> \
                <span class="main-text"><b>Selecione uma foto de perfil</b></span> <br /> \
                <span class="sub-text">(ou clique)</span> \
                ',
                maxFilesize: 2, // MB
                init: function() {
                    this.on("success", function(file, responseText) {
                        console.log(responseText);
                        //$('.dz-success-mark').hide();
                        //$('.dz-error-mark').show();
                    });
                }
            });
    });
</script>
</body>
</html>