<!DOCTYPE html>
<html>

<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>Miami Elite</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Font CSS (Via CDN) -->
<link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800'>
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300">
<!-- Dropzone CSS -->
<link rel="stylesheet" href="<?php PUBLIC_FOLDER_ACCESS(); ?>vendor/plugins/dropzone/downloads/css/dropzone.css">
<!-- Theme CSS -->
<link rel="stylesheet" type="text/css" href="<?php PUBLIC_FOLDER_ACCESS(); ?>assets/skin/default_skin/css/theme.css">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

</head>

<body class="form-uploaders-page">

 
    <div id="main">

       <header class="navbar navbar-fixed-top bg-light">
        <div class="navbar-branding">
            <a class="navbar-brand" href="dashboard.html"> <b>Miami</b>Elite </a>
            <span id="toggle_sidemenu_l" class="glyphicons glyphicons-show_lines"></span>
            <ul class="nav navbar-nav pull-right hidden">
                <li>
                    <a href="#" class="sidebar-menu-toggle">
                        <span class="octicon octicon-ruby fs20 mr10 pull-right "></span>
                    </a>
                </li>
            </ul>
        </div>

    </header>

    <?php template_admin_navigation($this->session->userdata('tipousuario')); ?>

        <!-- Start: Content -->
        <section id="content_wrapper">

    

        <section id="content">
        <div class="col-md-12">
            <div class="panel">
            <div class="panel-heading">
                <span class="panel-title">Cadastar Imóvel Destaque</span>
            </div>
            <div class="panel-body">
                <form class="form-horizontal" action="<?php siteurl(); ?>dashboard/featured_new_process" method="post" role="form">
                    <div class="form-group">
                        <div class="col-lg-12">
                            <label for="inputStandard" class="col-lg-3" style="margin-left:-5px">Cidade/Bairro</label>
                            <input type="text" name="cidade" id="cidade" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-6">
                            <label for="inputStandard" class="col-lg-3" style="margin-left:-5px">Quartos</label>
                            <input type="text" name="quartos" id="quartos" class="form-control">
                        </div>
                        <div class="col-lg-6">
                            <label for="inputStandard" class="col-lg-3" style="margin-left:-5px">Banheiros</label>
                            <input type="text" name="banheiros" id="banheiros" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <label for="inputStandard" class="col-lg-3" style="margin-left:-5px"> Descrição</label>
                            <textarea name="descricao" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-1">
                        <label for="inputStandard" class="col-lg-3 control-label"></label>
                            <input type="submit" value="Cadastrar" class="btn btn-primary">
                        </div>
                    </div>
                </form>
            </div>
            </div>
        </div>
        </section>
    </section>
    </div>
    <!-- End: Main -->

    <!-- jQuery -->
    <script type="text/javascript" src="<?php PUBLIC_FOLDER_ACCESS(); ?>vendor/jquery/jquery-1.11.1.min.js"></script>
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <!-- Bootstrap -->
    <script type="text/javascript" src="<?php PUBLIC_FOLDER_ACCESS(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
    <!-- Theme Javascript -->
    <script type="text/javascript" src="<?php PUBLIC_FOLDER_ACCESS(); ?>assets/js/utility/utility.js"></script>
    <script type="text/javascript" src="<?php PUBLIC_FOLDER_ACCESS(); ?>assets/js/main.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            "use strict";
            Core.init();

            <?php $result = ''; foreach($cityname as $name): $result .=  "'".$name."',"; endforeach; ?>
        var availableTags = [ <?php echo $result; ?>];
        function split( val ) { return val.split( /,\s*/ ); }
        function extractLast( term ) { return split( term ).pop() }
        $( "#cidade" )
          .bind( "keydown", function( event ) {
            if ( event.keyCode === $.ui.keyCode.TAB &&
                $( this ).autocomplete( "instance" ).menu.active ) {
              event.preventDefault();
            }
          })
          .autocomplete({
            minLength: 0,
            source: function( request, response ) {
                response( $.ui.autocomplete.filter(
                availableTags, extractLast( request.term ) ) );
            },
            focus: function() { return false; },
            select: function( event, ui ) {
                var terms = split( this.value );
                terms.pop();
                terms.push( ui.item.value );
                terms.push( "" );
                this.value = terms.join( ", " );
                return false;
            }
          });
        });
    </script>
    <!-- END: PAGE SCRIPTS -->

</body>

</html>