<?php
webCONFIG();
global $D;
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>MVP</title>
	<meta name="viewport" content="width=device-width" />
	<link rel="stylesheet" href="<?php echo $D->LIBRARY; ?>foundation/css/foundation.css" />
	<link rel="stylesheet" href="<?php echo $D->CSS_PATH; ?>theme.css" />
	<script src="<?php echo $D->LIBRARY; ?>foundation/js/vendor/modernizr.js"></script>
  	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
  	<script src="<?php echo $D->JS_PATH; ?>app.js"></script>
  	<script>siteurl='<?php echo $D->BASEURL; ?>'</script>
  	<link rel="stylesheet" type="text/css" href="<?php echo $D->LIBRARY; ?>slideshow/css/demo.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $D->LIBRARY; ?>slideshow/css/style1.css" />
	<script type="text/javascript" src="<?php echo $D->LIBRARY; ?>slideshow/js/modernizr.custom.86080.js"></script>
	<link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
</head>
<body>
	<ul class="cb-slideshow">
        <li><span>Image 01</span><div><h3></h3></div></li>
        <li><span>Image 02</span><div><h3></h3></div></li>
        <li><span>Image 03</span><div><h3></h3></div></li>
        <li><span>Image 04</span><div><h3></h3></div></li>
        <li><span>Image 05</span><div><h3></h3></div></li>
        <li><span>Image 06</span><div><h3></h3></div></li>
    </ul>


	<!-- content -->
	<div class="small-12 small-centered large-10 columns wide">
	<div class="nav">
		<div id="contact">
			<div id="item">305.373.0102</div>
				<div class="separator"></div>
			<div id="item"><img src="<?php echo $D->IMG_PATH; ?>email.png" width="16" alt="email contact"> email@email.com</div>
				<div class="separator"></div>
			<div id="item"><span>English</span></div>
		</div>
		<nav class="top-bar" data-topbar role="navigation">
			  <section class="top-bar-section">
			  	<ul class="center">
			      <li><a href="<?php echo $D->BASEURL; ?>dashboard">INICIO</a></li>
			      <li><a href="<?php echo $D->BASEURL; ?>dashboard/meusImoveis">MEUS IMÓVEIS</a></li>
			      <li><a href="#">CONTATOS</a></li>
			      <li><a href="<?php echo $D->BASEURL; ?>home/logout">SAIR</a></li>
			    </ul>
			  </section>
		</nav>
	</div>
	</div>

	<br/>
	<br/>
	<br/>
	<!-- SLIDESHOW -->


	<!-- content -->
	<div class="small-10 small-centered large-10 columns menu_search">
		<div id="menuSearch1" class="menu_active"><a href="javascript:void(0)" onclick="changeSearch('menuSearch1');">PESQUISA RÁPIDA</a></div>
		<div id="menuSearch2" class="menu"><a  href="javascript:void(0)" onclick="changeSearch('menuSearch2');">PESQUISA AVANÇADA</a></div>
	</div>
	<div class="small-10 small-centered large-10 columns display_table" id="container-results">

		<br/>
		<!-- search form -->
		<div class="small-12 large-12 columns display_table" style="padding:0">
		<form 	method="post"
				action="<?php echo $D->BASEURL; ?>dashboard/search">

			<div class="large-12 columns" id="container-results-form">
		      <div class="row collapse">
		        <div class="small-8 columns">
		          <input type="text" name="search" value="" style="background: #575f74; border:0; border-radius:3px 0 0 3px; color:white" placeholder="Pesquisa pelo ML# Number"/>
		        </div>
		         <div class="small-2 columns">
		          <select name="maxvalue">
		          	<option>Quantidade</option>
		          	<option>50</option>
		          	<option>100</option>
		          </select>
		        </div>
		       <div class="small-2 columns">
		          <button class="button postfix">IR</button>
		        </div>
		      </div>
		    </div>
		</form>
		</div>

		<!-- result search -->

		<!-- ads -->
		<div class="small-12 large-12 columns display_table" id="searchResult">
			<?php if(count($resultArray) < 1){ ?>
				<br/>
				<?php echo alert('info', '<strong>Nenhum imóvel selecionado até o momento!</strong>'); ?>
			<?php } else { ?>
			    <br/>
				<?php foreach ($resultArray as $result) { ?>
					<div style="color:#fff; border:0px solid #fff; height:200px; margin: 3px 0 3px 0; overflow:hidden">
						<div style="margin:10px; width:15%; border:1px solid #2c303b; float:left">
							<img src="<?php echo $D->UPLOADLINK; ?>photo_<?php echo $result['sysid']; ?>/1.jpg">
						</div>

						<div style="margin:5px; height:100px; width:80%; border:0px solid #fff; float:left">
							<h1><?php echo($result['field_61']); ?>, <?php echo $result['field_248']?> - <?php echo $result['field_10']?></h1>
							<?php echo($result['field_157']); ?><br/><br/>
							<div class="descriptionResult"><?php echo($result['field_1339']); ?></div>
							<div class="itemResult">quartos<br/><?php echo(!empty($result['field_25'])? $result['field_25'] : '-'); ?></div>
							<div class="itemResult" style="margin-left:5px">banheiros<br/>-</div>
							<div class="itemResult">área<br/><?php echo(!empty($result['field_19'])? $result['field_19'] : '-'); ?></div>

						</div>
					</div>
				<?php } ?>
			<?php } ?>


		</div>

	</div>
</body>
</html>

