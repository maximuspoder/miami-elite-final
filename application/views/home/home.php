<!doctype html>
<html lang="en-us">
<head>
<!--Page Title-->
<title>Miami Elite</title>
<!--Meta Tags-->
<meta charset="UTF-8">
<meta name="author" content="">
<meta name="keywords" content=""/>
<meta name="description" content=""/>
<!-- Set Viewport-->
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<link rel="stylesheet" href="<?php PUBLIC_FOLDER_ACCESS_OUT(); ?>css/bootstrap.min.css" type="text/css"/>
<link rel="stylesheet" href="<?php PUBLIC_FOLDER_ACCESS_OUT(); ?>css/bootstrap-theme.min.css" type="text/css"/>
<link rel="stylesheet" href="<?php PUBLIC_FOLDER_ACCESS_OUT(); ?>css/font-awesome.min.css">
<link rel="stylesheet" href="<?php PUBLIC_FOLDER_ACCESS_OUT(); ?>css/flexslider.css">
<link rel="stylesheet" href="<?php PUBLIC_FOLDER_ACCESS_OUT(); ?>css/select-theme-default.css">
<link rel="stylesheet" href="<?php PUBLIC_FOLDER_ACCESS_OUT(); ?>css/owl.carousel.css">
<link rel="stylesheet" href="<?php PUBLIC_FOLDER_ACCESS_OUT(); ?>css/owl.theme.css">
<link rel="stylesheet" href="<?php PUBLIC_FOLDER_ACCESS_OUT(); ?>css/style.css" type="text/css"/>
<link rel="stylesheet" href="<?php PUBLIC_FOLDER_ACCESS_OUT(); ?>font-awesome/css/font-awesome.min.css" type="text/css"/>
<!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
</head>
<body id='top'>
<header>
<div id="top-strip">
  <div class="container">
    <ul class="pull-left social-icons">
      <li><a href="index.html#" class="fa fa-twitter"></a></li>
      <li><a href="index.html#" class="fa fa-facebook"></a></li>
    </ul>
    <div id="login-box" class='pull-right'>
      <a href="<?php url_to('home/login'); ?>"><span>Entrar</span></a>
      <a href="<?php url_to('home/register'); ?>"><span>Cadastro</span></a>
    </div>
  </div>
</div>
</header>
<!-- /Header -->
<div class="slider-section">
  <div id="premium-bar">
    <div class="container">
      <nav class="navbar navbar-default" role="navigation">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo url_to('home/index'); ?>"><img src="<?php PUBLIC_FOLDER_ACCESS_OUT(); ?>img/logo.png" alt="logo"></a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <?php template_navigation() ?>        
      </div>
      <!-- /.container-fluid -->
      </nav>
    </div>
  </div>
  <!-- Slider-Section -->
  <div class="main-flexslider" style="height:510px">
    <ul class="slides">
      <?php foreach($imoveis as $result):?>
      <li class='slides' id='slide-n1'>
        <img src="<?php echo FEATUREDLINK.'featured_'.$result->iddestaque.'/'.$result->foto; ?>" alt="slideshow">
      </li>
      <?php endforeach; ?>
    </ul>
  </div>
</div>
<?php template_search(); ?>

<!-- Recent-Listings-Section -->
<div class="recent-listings">
  <div class="container">
    <div class="title-box">
      <h3>Imóveis</h3>
      <div class="bordered">
      </div>
    </div>
    <div class="row listings-items-wrapper">
      <?php foreach($imoveis as $result):?>
      <div class="col-md-4 listing-single-item">
        <div class="item-inner">
          <div class="image-wrapper">
            <img src="<?php echo FEATUREDLINK.'featured_'.$result->iddestaque.'/'.$result->foto; ?>" height="289" alt="gallery">
          </div>
          <div class="desc-box">
            <h4><a href="javascript:void(0)"><?php echo  substr($result->cidade,0, -2); ?></a></h4>
            <ul class="slide-item-features item-features">
              <li><span class="fa fa-male"></span><?php echo $result->banheiros; ?> Banheiro(s)</li>
              <li><span class="fa fa-inbox"></span><?php echo $result->quartos; ?> Quarto(s)</li>
            </ul> 
            <div class="buttons-wrapper">
              <a href="<?php echo url_to("home/details/{$result->iddestaque}/featured")?>" class="gray-btn"><span class="fa fa-file-text-o"></span>Detalhes</a>
            </div>
            <div class="clearfix">
            </div>
          </div>
        </div>
      </div>
      <?php endforeach;?>
    </div>
  </div>
</div>

<?php if($this->session->userdata('subdominio') != 'http://miamielite.com.br'){ ?>
<!-- Agents-Section -->
<div class="agents-section">
  <div class="container">
    <div class="title-box">
      <h3>Corretor</h3>
      <div class="bordered">
      </div>
    </div>
    <div class="owl-carousel agents-slider">
      <div class="single-agent">
        <div class="image-box">
          <img src="<?php echo $foto_perfil; ?>" alt="agent">
        </div>
        <div class="desc-box">
          <h4><?php echo $this->session->userdata('nome_completo'); ?></h4>
          <p class="person-number">
            <i class="fa fa-phone"></i> <?php echo $this->session->userdata('telefone'); ?>
          </p>
          <p class="person-email">
            <i class="fa fa-envelope"></i> <?php echo $this->session->userdata('email'); ?>
          </p>
        </div>
      </div>
    </div>
  </div>
</div>
<?php } ?>
<?php template_footer($this->session->userdata('telefone'), $this->session->userdata('email')) ?>
<!-- Javascript -->
<script type="text/javascript" src="<?php PUBLIC_FOLDER_ACCESS_OUT(); ?>js/jquery-2.1.0.min.js"></script>
<script type="text/javascript" src="<?php PUBLIC_FOLDER_ACCESS_OUT(); ?>js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php PUBLIC_FOLDER_ACCESS_OUT(); ?>js/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="<?php PUBLIC_FOLDER_ACCESS_OUT(); ?>js/select.min.js"></script>
<script type="text/javascript" src="<?php PUBLIC_FOLDER_ACCESS_OUT(); ?>js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php PUBLIC_FOLDER_ACCESS_OUT(); ?>js/script.js"></script>
</body>
</html>