<!doctype html>
<html lang="en-us">
<head>
<!--Page Title-->
<title>Miami Elite</title>
<!--Meta Tags-->
<meta charset="UTF-8">
<meta name="author" content="">
<meta name="keywords" content=""/>
<meta name="description" content=""/>
<!-- Set Viewport-->
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<link rel="stylesheet" href="<?php PUBLIC_FOLDER_ACCESS_OUT(); ?>css/bootstrap.min.css" type="text/css"/>
<link rel="stylesheet" href="<?php PUBLIC_FOLDER_ACCESS_OUT(); ?>css/bootstrap-theme.min.css" type="text/css"/>
<link rel="stylesheet" href="<?php PUBLIC_FOLDER_ACCESS_OUT(); ?>css/font-awesome.min.css">
<link rel="stylesheet" href="<?php PUBLIC_FOLDER_ACCESS_OUT(); ?>css/flexslider.css">
<link rel="stylesheet" href="<?php PUBLIC_FOLDER_ACCESS_OUT(); ?>css/select-theme-default.css">
<link rel="stylesheet" href="<?php PUBLIC_FOLDER_ACCESS_OUT(); ?>css/owl.carousel.css">
<link rel="stylesheet" href="<?php PUBLIC_FOLDER_ACCESS_OUT(); ?>css/owl.theme.css">
<link rel="stylesheet" href="<?php PUBLIC_FOLDER_ACCESS_OUT(); ?>css/style.css" type="text/css"/>
<link rel="stylesheet" href="<?php PUBLIC_FOLDER_ACCESS_OUT(); ?>font-awesome/css/font-awesome.min.css" type="text/css"/>
<!--[if IE]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
</head>
<body id='top' class="property-details-page">
<header>
<div id="top-strip">
  <div class="container">
    <ul class="pull-left social-icons">
      <li><a href="index.html#" class="fa fa-twitter"></a></li>
      <li><a href="index.html#" class="fa fa-facebook"></a></li>
    </ul>
    <div id="login-box" class='pull-right'>
      <a href="<?php url_to('home/login'); ?>"><span>Entrar</span></a>
      <a href="<?php url_to('home/register'); ?>"><span>Cadastro</span></a>
    </div>
  </div>
</div>
</header>
<!-- /Header -->
<div class="slider-section">
	<div id="premium-bar">
		<div class="container">
			<nav class="navbar navbar-default" role="navigation">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="<?php url_to('home/index') ?>"><img src="<?php PUBLIC_FOLDER_ACCESS_OUT(); ?>img/logo.png" alt="logo"></a>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<?php template_navigation() ?>
				<!-- /.navbar-collapse -->
			</div>
			<!-- /.container-fluid -->
			</nav>
		</div>
	</div>
	<!-- head-Section -->
	<div class="page-title-section">
		<div class="container">
			
		</div>
	</div>
</div>
<!-- Search-Section -->
<?php template_search() ?>
<!-- content-Section -->
<div class="content-section">
	<div class="container">
		<div class="row">
			<div class="col-md-8 page-content">
				<div class="inner-wrapper">
					<div class="property-images-slider">
						<div id="details-slider" class="flexslider">

						<?php
							$photos = array();
			                if(file_exists(PHOTOS .'photos_'.$sysid.'/1.jpg')){
			                    $photos[0] = UPLOADLINK . 'photos_'.$sysid.'/1.jpg';
			                }
			                if(file_exists(PHOTOS .'photos_'.$sysid.'/2.jpg')){
			                    $photos[1] = UPLOADLINK . 'photos_'.$sysid.'/2.jpg';
			                }
			                if(file_exists(PHOTOS .'photos_'.$sysid.'/3.jpg')){
			                    $photos[2] = UPLOADLINK . 'photos_'.$sysid.'/3.jpg';
			                }
			                if(file_exists(PHOTOS .'photos_'.$sysid.'/4.jpg')){
			                    $photos[3] = UPLOADLINK . 'photos_'.$sysid.'/4.jpg';
			                }
			                if(file_exists(PHOTOS .'photos_'.$sysid.'/5.jpg')){
			                    $photos[4] = UPLOADLINK . 'photos_'.$sysid.'/5.jpg';
			                }
			                if(file_exists(PHOTOS .'photos_'.$sysid.'/6.jpg')){
			                    $photos[5] = UPLOADLINK . 'photos_'.$sysid.'/6.jpg';
			                }
			                if(file_exists(PHOTOS .'photos_'.$sysid.'/7.jpg')){
			                    $photos[6] = UPLOADLINK . 'photos_'.$sysid.'/7.jpg';
			                }
			                if(file_exists(PHOTOS .'photos_'.$sysid.'/7.jpg')){
			                    $photos[7] = UPLOADLINK . 'photos_'.$sysid.'/7.jpg';
			                }

		                ?>
							<ul class="slides">
								<?php 
								if(count($photos) > 0){
									foreach($photos as $foto){ ?>
									<li>
									<div class="image-wrapper">
										<img src="<?php echo @$foto; ?>" alt="gallery">
									</div>
									</li>
								<?php 
									} 
								} else {
								?>
								<li>
									<div class="image-wrapper">
										<img src="<?php echo UPLOADLINK; ?>home.png" height="400" alt="Sem imagem">
									</div>
								</li>
								<?php
								}
								?>
							</ul>
						</div>
						<div id="details-carousel" class="flexslider">
							<ul class="slides">
								<?php 
								if(count($photos) > 0){
									foreach($photos as $foto){ ?>
								<li>
								<img src="<?php echo @$foto; ?>" height="100" alt="gallery">
								</li>
								<?php 
									}
								}
								?>
							</ul>
						</div>
					</div>
					<div class="property-desc">
						<h3><?php echo substr(@$imovel[0]->cidade, 0, -2); ?></h3>
						<?php
			                foreach($imovel[0] as $key => $data):
			                ?>
                            <div class="col-md-10" style="font-size:14px;">
                                <?php 
                                	$field = explode('_', $key);
                                	if(isset($field[1])){
                                		$map = map(strtolower($imovel[0]['property_type']), $field[1]);
                                		if($map != ""){
                                			echo $map.': ';
                                			echo '<strong> '.$data.'</strong><br/>';	
                                		}
                                		
                                	}
                                ?>
                            </div>
			                <?php
			                endforeach;
			            ?>
					</div>
				</div>
			</div>
			<div class="col-md-4 blog-sidebar">
				<div class="sidebar-widget author-profile">
					<h4 class="widget-title">Corretor</h4>
					<div class="image-box">
						<img src="<?php echo $foto_perfil; ?>" alt="agent">
					</div>
					<div class="desc-box">
						<h4><?php echo @$this->session->userdata('nome_completo'); ?></h4>
						<p class="person-number">
							<i class="fa fa-phone"></i> <?php echo @$this->session->userdata('telefone'); ?>
						</p>
						<p class="person-email">
							<i class="fa fa-envelope"></i> <?php echo @$this->session->userdata('email'); ?>
						</p>
					</div>
				</div>
				
				<div class="sidebar-widget text-widget">
					<h4 class="widget-title">Miami Elite</h4>
					<p class='first-paragraph'>
						Texto sobre a miami elite aqui
					</p>

				</div>
			</div>
		</div>
	</div>
</div>
<?php template_footer($this->session->userdata('telefone'), $this->session->userdata('email')) ?>

<!-- Javascript -->
<script type="text/javascript" src="<?php PUBLIC_FOLDER_ACCESS_OUT(); ?>js/jquery-2.1.0.min.js"></script>
<script type="text/javascript" src="<?php PUBLIC_FOLDER_ACCESS_OUT(); ?>js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php PUBLIC_FOLDER_ACCESS_OUT(); ?>js/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="<?php PUBLIC_FOLDER_ACCESS_OUT(); ?>js/select.min.js"></script>
<script type="text/javascript" src="<?php PUBLIC_FOLDER_ACCESS_OUT(); ?>js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php PUBLIC_FOLDER_ACCESS_OUT(); ?>https://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="<?php PUBLIC_FOLDER_ACCESS_OUT(); ?>js/script.js"></script>
</body>
</html>