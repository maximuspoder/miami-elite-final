<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php SITE_TITLE(); ?></title>
    <meta name="keywords" content="" />
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300">
    <link rel="stylesheet" href="<?php PUBLIC_FOLDER_ACCESS(); ?>assets/skin/default_skin/css/theme.css">
    <link rel="stylesheet" href="<?php PUBLIC_FOLDER_ACCESS(); ?>assets/admin-tools/admin-forms/css/admin-forms.css">
    <script>siteurl = '<?php siteurl() ?>'</script>
</head>

<body class="external-page sb-l-c sb-r-c">
<script>
    var boxtest = localStorage.getItem('boxed');
        if (boxtest === 'true') { document.body.className += ' boxed-layout'; }
</script>
    <div id="main" class="animated fadeIn">
        <section id="content_wrapper">
            <div id="canvas-wrapper">
                <canvas id="demo-canvas"></canvas>
            </div>
 

 <section id="content">
    <div class="admin-form theme-info mw700" style="margin-top: 3%;" id="login1">
        <div class="row mb15 table-layout">
            <div class="col-xs-6 va-m pln">
                <a href="javascript:void(0)" title="Return to Dashboard">
                    <img src="<?php PUBLIC_FOLDER_ACCESS(); ?>assets/img/miamielite.png" alt="Miami Elite" class="img-responsive w250">
                </a>
            </div>

            <div class="col-xs-6 text-right va-b pr5">
                <div class="login-links">
                    <a href="<?php url_to('home/login'); ?>" class="" title="Sign In">Entrar</a>
                    <span class="text-white"> | </span>
                    <a href="<?php url_to('home/register'); ?>" class="active" title="Register">Criar Conta</a>
                </div>
            </div>
        </div>
        <div class="panel panel-info mt10 br-n">
            <form method="post" action="<?php url_to('home/doRegister'); ?>" name="admin-form" id="admin-form" novalidate="novalidate">
                <div class="panel-body p25 bg-light">
                    <div class="section-divider mt10 mb40">
                        <span>Criar Conta</span>
                    </div>

                    <div class="section">
                            <label for="subdomain" class="field prepend-icon">
                                <input type="text" name="subdomain"  value="" id="subdomain" class="gui-input" placeholder="http://seudominio.miamielite.com.br">
                                <label for="subdomain" class="field-icon"><i class="fa fa-cog"></i>
                                </label>
                            <div id="putSubdomainError"></div>
                            </label>
                    </div>

                    <div class="section row">
                        <div class="col-md-12">
                            <label for="name" class="field prepend-icon">
                                <input type="text" name="name" id="name" class="gui-input" placeholder="Nome">
                                <label for="name" class="field-icon"><i class="fa fa-user"></i>
                                </label>
                            </label>
                        </div>
                    </div>

                    <div class="section row">
                        <div class="col-md-12">
                            <label for="name" class="field prepend-icon">
                                <input type="text" name="phone" id="phone" class="gui-input" placeholder="Telefone">
                                <label for="name" class="field-icon"><i class="fa fa-phone"></i>
                                </label>
                            </label>
                        </div>
                    </div>

                    <div class="section">
                        <label for="creci" class="field prepend-icon">
                            <input type="text" name="creci" id="creci" class="gui-input" placeholder="Creci">
                            <label for="creci" class="field-icon"><i class="fa fa-star"></i>
                            </label>
                        </label>
                    </div>

                    <div class="section row">
                        <div class="col-md-6">
                            <label for="email" class="field prepend-icon">
                                <input type="text" style="display:none" name="mail" id="mail" class="gui-input" placeholder="Email">
                                <input type="password"style="display:none" name="password" id="password" class="gui-input" placeholder="Email">
                                <input type="text" name="email" id="email" class="gui-input" placeholder="Email">
                                <label for="email" class="field-icon"><i class="fa fa-envelope"></i>
                                </label>
                                <div id="putEmailError"></div>
                            </label>
                        </div>

                        <div class="col-md-6">
                            <label for="password" class="field prepend-icon">
                                <input type="password" name="password" id="password" class="gui-input" placeholder="Senha">
                                <label for="password" class="field-icon"><i class="fa fa-lock"></i>
                                </label>
                            </label>
                        </div>
                    </div>

                </div>
                <div class="panel-footer clearfix">
                    <button type="submit" class="button btn-primary pull-right">Criar Conta</button>
                </div>
            </form>
        </div>
    </div>
</section>



        </section>
    </div>

<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script src="<?php PUBLIC_FOLDER_ACCESS(); ?>vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="<?php PUBLIC_FOLDER_ACCESS(); ?>vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
<script src="<?php PUBLIC_FOLDER_ACCESS(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
<script src="<?php PUBLIC_FOLDER_ACCESS(); ?>assets/js/pages/login/EasePack.min.js"></script>
<script src="<?php PUBLIC_FOLDER_ACCESS(); ?>assets/admin-tools/admin-forms/js/jquery.validate.min.js"></script>
<script src="<?php PUBLIC_FOLDER_ACCESS(); ?>assets/admin-tools/admin-forms/js/additional-methods.min.js"></script>
<script src="<?php PUBLIC_FOLDER_ACCESS(); ?>assets/js/custom.js"></script>
</body>