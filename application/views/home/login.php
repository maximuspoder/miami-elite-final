<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php SITE_TITLE(); ?></title>
    <meta name="keywords" content="" />
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300">
    <link rel="stylesheet" href="<?php PUBLIC_FOLDER_ACCESS(); ?>assets/skin/default_skin/css/theme.css">
    <link rel="stylesheet" href="<?php PUBLIC_FOLDER_ACCESS(); ?>assets/admin-tools/admin-forms/css/admin-forms.css">
    <script>siteurl = '<?php siteurl() ?>'</script>
</head>

<body class="external-page sb-l-c sb-r-c">
<script>
    var boxtest = localStorage.getItem('boxed');
    if (boxtest === 'true') { document.body.className += ' boxed-layout'; }
</script>
<div id="main" class="animated fadeIn">
    <section id="content_wrapper">
        <div id="canvas-wrapper">
            <canvas id="demo-canvas"></canvas>
        </div>

<section id="content">
    <div class="admin-form theme-info" id="login1">
        <div class="row mb15 table-layout">
            <div class="col-xs-6 va-m pln">
               <a href="javascript:void(0)" title="Return to Dashboard">
                   <img src="<?php PUBLIC_FOLDER_ACCESS(); ?>assets/img/miamielite.png" alt="Miami Elite" class="img-responsive w250">
                </a>
            </div>
            <div class="col-xs-6 text-right va-b pr5">
                <div class="login-links">
                    <a href="<?php url_to('home/login'); ?>" class="" title="Sign In">Entrar</a>
                    <span class="text-white"> | </span>
                    <a href="<?php url_to('home/register'); ?>" class="active" title="Register">Criar Conta</a>
                </div>
            </div>
        </div>
        <div class="panel panel-info mt10 br-n">
            <form method="post" action="<?php url_to('home/doLogin'); ?>" id="login">
                <div class="panel-body bg-light p30">
                    <div class="row">
                        <div class="col-sm-7 pr30">
                            <div class="section">
                                <label for="email" class="field-label text-muted fs18 mb10">Email</label>
                                <label for="email" class="field prepend-icon">
                                    <input type="email" name="email" id="email" class="gui-input" placeholder="Email">
                                    <label for="email" class="field-icon"><i class="fa fa-user"></i>
                                    </label>
                                </label>
                            </div>
                            <div class="section">
                                <label for="password" class="field-label text-muted fs18 mb10">Senha</label>
                                <label for="password" class="field prepend-icon">
                                    <input type="password" name="password" id="password" class="gui-input" placeholder="Senha">
                                    <label for="password" class="field-icon"><i class="fa fa-lock"></i>
                                    </label>
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-5 br-l br-grey pl30">
                            <h3 class="mb25"> Administre seus imóveis:</h3>
                            <p class="mb15">
                                <span class="fa fa-check text-success pr5"></span> Escolha seu domínio</p>
                            <p class="mb15">
                                <span class="fa fa-check text-success pr5"></span> Escolha os imóveis</p>
                            <p class="mb15">
                                <span class="fa fa-check text-success pr5"></span> Monte sua vitrine</p>
                            <p class="mb15">
                                <span class="fa fa-check text-success pr5"></span> Venda</p>
                        </div>
                    </div>
                </div>
                <div class="panel-footer clearfix p10 ph15">
                    <button type="submit" class="button btn-primary mr10 pull-right">Entrar</button>
                    <label class="switch block switch-primary pull-left input-align mt10">
                    </label>
                </div>
            </form>
        </div>
    </div>
</section>


    </section>
</div>

<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script src="<?php PUBLIC_FOLDER_ACCESS(); ?>vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="<?php PUBLIC_FOLDER_ACCESS(); ?>vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
<script src="<?php PUBLIC_FOLDER_ACCESS(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
<script src="<?php PUBLIC_FOLDER_ACCESS(); ?>assets/js/pages/login/EasePack.min.js"></script>
<script src="<?php PUBLIC_FOLDER_ACCESS(); ?>assets/admin-tools/admin-forms/js/jquery.validate.min.js"></script>
<script src="<?php PUBLIC_FOLDER_ACCESS(); ?>assets/admin-tools/admin-forms/js/additional-methods.min.js"></script>
</body>