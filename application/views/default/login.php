<?php require VIEW_PATH.'_template/public/header.php'; ?>
<body>
	<ul class="cb-slideshow">
        <li><span>Image 01</span><div><h3></h3></div></li>
        <li><span>Image 02</span><div><h3></h3></div></li>
        <li><span>Image 03</span><div><h3></h3></div></li>
        <li><span>Image 04</span><div><h3></h3></div></li>
        <li><span>Image 05</span><div><h3></h3></div></li>
        <li><span>Image 06</span><div><h3></h3></div></li>
    </ul>

    <?php require VIEW_PATH.'_template/public/menu.php'; ?>

	<div class="small-6 small-centered large-6 columns" id="container-register">
		<div id="header"><center><span>Realizar Login</span></center></div>
		<br/>

		<form action="login_process" method="post">
			<br/>
			<div class="row">
		    <div class="large-12 columns">
		      <label>Subdomínio
		        <input type="text" name="subdominio" value="<?php echo $domain; ?>" placeholder="Subdomínio" />
		      </label>
		    </div>
		  </div>

		  <div class="row">
		    <div class="large-12 columns">
		      <label>Email
		        <input type="email" name="mail" placeholder="Email" required/>
		      </label>
		    </div>
		  </div>

		  <div class="row">
		  	<div class="large-12 columns">
		      <label>Senha
		        <input type="password" name="password" placeholder="Senha" required/>
		      </label>
		    </div>
		</div>

		  <div class="row">
		    <div class="large-12 columns">
		      <label>
		        <button class="btn tiny btn-default">Entrar</button>
		      </label>
		    </div>
		  </div>

		 </form>
	</div>

</body>
</html>
