<?php
webCONFIG();
global $D;
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>MVP</title>
	<meta name="viewport" content="width=device-width" />
	<link rel="stylesheet" href="<?php echo $D->LIBRARY; ?>foundation/css/foundation.css" />
	<link rel="stylesheet" href="<?php echo $D->CSS_PATH; ?>theme.css" />
	<script src="<?php echo $D->LIBRARY; ?>foundation/js/vendor/modernizr.js"></script>
  	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
  	<link rel="stylesheet" type="text/css" href="<?php echo $D->LIBRARY; ?>slideshow/css/demo.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $D->LIBRARY; ?>slideshow/css/style1.css" />
	<script type="text/javascript" src="<?php echo $D->LIBRARY; ?>slideshow/js/modernizr.custom.86080.js"></script>
</head>
<body>
	<ul class="cb-slideshow">
        <li><span>Image 01</span><div><h3></h3></div></li>
        <li><span>Image 02</span><div><h3></h3></div></li>
        <li><span>Image 03</span><div><h3></h3></div></li>
        <li><span>Image 04</span><div><h3></h3></div></li>
        <li><span>Image 05</span><div><h3></h3></div></li>
        <li><span>Image 06</span><div><h3></h3></div></li>
    </ul>


	<!-- content -->
	<div class="small-12 small-centered large-10 columns wide">
	<div class="nav">
		<div id="contact">
			<div id="item">305.373.0102</div>
				<div class="separator"></div>
			<div id="item"><img src="<?php echo $D->IMG_PATH; ?>email.png" width="16" alt="email contact"> email@email.com</div>
				<div class="separator"></div>
			<div id="item"><span>English</span></div>
		</div>
		<nav class="top-bar" data-topbar role="navigation">
			  <section class="top-bar-section">
			  	<ul class="center">
			      <li><a href="<?php echo $D->BASEURL; ?>">INICIO</a></li>
			      <li><a href="#">PESQUISA DE IMÓVEIS</a></li>
			      <li><a href="#">CONDOMÍNIOS</a></li>
			      <li><a href="#">MAPA DA ÁREA</a></li>
			      <li><a href="#">VÍDEOS</a></li>
			      <li><a href="#">FOTOS</a></li>
			      <li><a href="<?php echo $D->BASEURL; ?>home/register">CADASTRO</a></li>
			      <li><a href="<?php echo $D->BASEURL; ?>home/login">LOGIN</a></li>
			      <li><a href="<?php echo $D->BASEURL; ?>home/fale_conosco">FALE CONOSCO</a></li>
			    </ul>
			  </section>
		</nav>
	</div>
	</div>

	<br/>
	<br/>
	<br/>
	<!-- SLIDESHOW -->


	<!-- content -->
	<div class="small-10 small-centered large-10 columns display_table" id="container-results">

		<br/>
		<!-- search form -->
		<div class="small-9 large-9 columns display_table" style="padding:0">
		<form method="post" action="google.com.br">

			<div class="large-12 columns" id="container-results-form">
		      <div class="row collapse">
		        <div class="small-10 columns">
		          <input type="text" style="background: #575f74; border:0; border-radius:3px 0 0 3px; color:white" placeholder="Digite um nome, prédio, endereço, bairro, CEP, MLS#"  />
		        </div>
		       <div class="small-2 columns">
		          <button class="button postfix">IR</button>
		        </div>
		      </div>
		    </div>

		</form>
		</div>

		<!-- result search -->
		<div class="small-3 large-3 columns display_table" id="container-count">
			<div id="container-count-header"><center>resultados disponíveis no site</center></div>
			<div id="result">
				<center>109.198</center>
			</div>
		</div>

		<br />
		<!-- ads -->
		<div class="small-12 large-12 columns display_table" id="container-ads">

		</div>

		<!-- sub container -->
		<div class="small-12 large-12 columns display_table" id="sub-container">

			<div class="small-8 large-8 columns display_table" id="highlights">
				<div class="row">
					<div class="small-12 large-12 columns display_table">
						<h4>ÁREAS EM DESTAQUE</h4>
					</div>
				</div>

				<div class="row">
					<div class="small-6 large-6 columns display_table">
						<img src="<?php echo $D->IMG_PATH; ?>example.png" alt="">
						<div class="small-6 large-6 columns display_table description_left">
							Description Title
						</div>
						<div class="small-6 large-6 columns display_table description_right">
							Categoria 1<br>
							Categoria 2
						</div>
					</div>
					<div class="small-6 large-6 columns display_table">
						<img src="<?php echo $D->IMG_PATH; ?>example.png" alt="">
						<div class="small-6 large-6 columns display_table description_left">
							Description Title
						</div>
						<div class="small-6 large-6 columns display_table description_right">
							Categoria 1<br>
							Categoria 2
						</div>
					</div>
				</div>

				<br><br>
				<div class="row">
					<div class="small-6 large-6 columns display_table">
						<img src="<?php echo $D->IMG_PATH; ?>example.png" alt="">
						<div class="small-6 large-6 columns display_table description_left">
							Description Title
						</div>
						<div class="small-6 large-6 columns display_table description_right">
							Categoria 1<br>
							Categoria 2
						</div>
					</div>
					<div class="small-6 large-6 columns display_table">
						<img src="<?php echo $D->IMG_PATH; ?>example.png" alt="">
						<div class="small-6 large-6 columns display_table description_left">
							Description Title
						</div>
						<div class="small-6 large-6 columns display_table description_right">
							Categoria 1<br>
							Categoria 2
						</div>
					</div>
				</div>

				<br><br>
				<div class="row">
					<div class="small-6 large-6 columns display_table">
						<img src="<?php echo $D->IMG_PATH; ?>example.png" alt="">
						<div class="small-6 large-6 columns display_table description_left">
							Description Title
						</div>
						<div class="small-6 large-6 columns display_table description_right">
							Categoria 1<br>
							Categoria 2
						</div>
					</div>
					<div class="small-6 large-6 columns display_table">
						<img src="<?php echo $D->IMG_PATH; ?>example.png" alt="">
						<div class="small-6 large-6 columns display_table description_left">
							Description Title
						</div>
						<div class="small-6 large-6 columns display_table description_right">
							Categoria 1<br>
							Categoria 2
						</div>
					</div>
				</div>

				<br><br>
				<div class="row">
					<div class="small-6 large-6 columns display_table">
						<img src="<?php echo $D->IMG_PATH; ?>example.png" alt="">
						<div class="small-6 large-6 columns display_table description_left">
							Description Title
						</div>
						<div class="small-6 large-6 columns display_table description_right">
							Categoria 1<br>
							Categoria 2
						</div>
					</div>
					<div class="small-6 large-6 columns display_table">
						<img src="<?php echo $D->IMG_PATH; ?>example.png" alt="">
						<div class="small-6 large-6 columns display_table description_left">
							Description Title
						</div>
						<div class="small-6 large-6 columns display_table description_right">
							Categoria 1<br>
							Categoria 2
						</div>
					</div>
				</div>

			</div>



			<!-- WIDGETS -->
			<div class="small-4 large-4 columns display_table" id="nav">

				<div class="small-12 large-12 columns display_table nav-filter" style="margin-top:45px">
					<div class="nav-filter-header">CONDOMÍNIOS DE LUXO</div>
					<div class="nav-filter-body">
					<select>
						<option>Escolha</option>
					</select>
					</div>
				</div>

				<div class="small-12 large-12 columns display_table nav-filter">
					<div class="nav-filter-header">COMUNIDADES DE LUXO</div>
					<div class="nav-filter-body">
					<select>
						<option>Escolha</option>
					</select>
					</div>
				</div>

				<div class="small-12 large-12 columns display_table nav-filter">
					<div class="nav-filter-header">NOVA CONSTRUÇÃO</div>
					<div class="nav-filter-body">

					</div>
				</div>

				<div class="small-12 large-12 columns display_table nav-filter">
					<div class="nav-filter-header">PESQUISA POR MAPA</div>
					<div class="nav-filter-body">

					</div>
				</div>

				<div class="small-12 large-12 columns display_table nav-filter" style="display:none">
					<div class="nav-filter-header">RESULTADOS <span>EXCLUSIVOS</span></div>
					<div class="nav-filter-header">MIAMI ELITE <span>AGENTES</span></div>
					<div class="nav-filter-header">SERVIÇOS</div>
					<div class="nav-filter-header">NA <span>IMPRENSA</span></div>
					<div class="nav-filter-header">SOBRE <span>PIQUET REALTY</span></div>
					<div class="nav-filter-header">PROPRIEDADES DO BANCO & <span>SHORT SALE</span></div>
					<div class="nav-filter-header">OPORTUNIDADES DE <span>CARREIRA</span></div>
					<div class="nav-filter-header">FEEDBACK DO <span>CLIENTE</span></div>
				</div>
			</div>



		</div>

		<!-- area -->
		<div class="small-12 large-12 columns display_table" id="container-area">
			<div class="small-12 large-12 columns display_table" id="highlights">
				<br/>
				<div class="small-12 large-12 columns display_table">
					<h5>ESCOLHA SUA ÁREA</h5>
				</div>

				<div class="row">
					<div class="small-6 large-4 columns display_table no_padding">
						<div class="container-area-img">
							<img src="<?php echo $D->IMG_PATH; ?>example1.png" alt="">
						</div>
						<div class="container-area-text">
							<span>MIAMI</span><br><br>
							Lorem ipsum dolor sit amet, 
							consectetur adipisicing elit, 
							sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua.
						</div>
					</div>
					<div class="small-6 large-4 columns display_table no_padding">
						<div class="container-area-img">
							<img src="<?php echo $D->IMG_PATH; ?>example1.png" alt="">
						</div>
						<div class="container-area-text">
							<span>MIAMI</span><br><br>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua.
						</div>
					</div>
					<div class="small-6 large-4 columns display_table no_padding">
						<div class="container-area-img">
							<img src="<?php echo $D->IMG_PATH; ?>example1.png" alt="">
						</div>
						<div class="container-area-text">
							<span>MIAMI</span><br><br>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua.
						</div>
					</div>
				</div>
		</div>
	</div>

	<!-- area -->
	<div class="small-12 large-12 columns display_table" id="container-blog">
		<div class="small-12 large-12 columns display_table" id="highlights">
			<br/>
			<div class="small-12 large-12 columns display_table">
				<h5>IMÓVEIS BLOG</h5>
			</div>

			<div class="row">
				<div class="small-6 large-4 columns display_table no_padding">
					<span>Imóveis em William Islands</span><br><br>
					Lorem ipsum dolor sit amet, 
					consectetur adipisicing elit, 
					sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua.
				</div>

				<div class="small-6 large-4 columns display_table no_padding">
					<span>Imóveis em William Islands</span><br><br>
					Lorem ipsum dolor sit amet, 
					consectetur adipisicing elit, 
					sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua.
				</div>

				<div class="small-6 large-4 columns display_table no_padding">
					<span>Imóveis em William Islands</span><br><br>
					Lorem ipsum dolor sit amet, 
					consectetur adipisicing elit, 
					sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua.
				</div>	
			</div>
	</div>
	</div>



</body>
</html>
