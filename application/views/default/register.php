<?php
webCONFIG();
global $D;
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>MVP</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<link rel="stylesheet" href="<?php echo $D->LIBRARY; ?>foundation/css/foundation.css" />
	<link rel="stylesheet" href="<?php echo $D->CSS_PATH; ?>theme.css" />
	<link rel="stylesheet" href="<?php echo $D->CSS_PATH; ?>app.css" />
	<script src="<?php echo $D->LIBRARY; ?>foundation/js/vendor/modernizr.js"></script>
  	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
  	<link rel="stylesheet" type="text/css" href="<?php echo $D->LIBRARY; ?>slideshow/css/demo.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $D->LIBRARY; ?>slideshow/css/style1.css" />
	<script type="text/javascript" src="<?php echo $D->LIBRARY; ?>slideshow/js/modernizr.custom.86080.js"></script>
	<script type="text/javascript" src="<?php echo $D->JS_PATH; ?>jquery.validationengine.js"></script>
	<script type="text/javascript" src="<?php echo $D->JS_PATH; ?>jquery.validationengine.pt_BR.js"></script>
	<script type="text/javascript" src="<?php echo $D->JS_PATH; ?>jquery.meiomask.js"></script>
	<style>
	select{background-color: #fff; color: #666;}
	</style>
	<script>
	$(document).ready(function(){
 		$("#form_default").validationEngine({ inlineValidation:false , promptPosition : "centerRight", scroll : false });
        $("input:text").setMask();
	})
	</script>
</head>
<body>
	<ul class="cb-slideshow">
        <li><span>Image 01</span><div><h3></h3></div></li>
        <li><span>Image 02</span><div><h3></h3></div></li>
        <li><span>Image 03</span><div><h3></h3></div></li>
        <li><span>Image 04</span><div><h3></h3></div></li>
        <li><span>Image 05</span><div><h3></h3></div></li>
        <li><span>Image 06</span><div><h3></h3></div></li>
    </ul>

    <!-- content -->
	<div class="small-12 small-centered large-10 columns wide">
	<div class="nav">
		<div id="contact">
			<div id="item">305.373.0102</div>
				<div class="separator"></div>
			<div id="item"><img src="<?php echo $D->IMG_PATH; ?>email.png" width="16" alt="email contact"> email@email.com</div>
				<div class="separator"></div>
			<div id="item"><span>English</span></div>
		</div>
		<nav class="top-bar" data-topbar role="navigation">
			  <section class="top-bar-section">
			  	<ul class="center">
			      <li><a href="<?php echo $D->BASEURL; ?>">INICIO</a></li>
			      <li><a href="#">PESQUISA DE IMÓVEIS</a></li>
			      <li><a href="#">CONDOMÍNIOS</a></li>
			      <li><a href="#">MAPA DA ÁREA</a></li>
			      <li><a href="#">VÍDEOS</a></li>
			      <li><a href="#">FOTOS</a></li>
			      <li><a href="<?php echo $D->BASEURL; ?>home/register">CADASTRO</a></li>
			      <li><a href="<?php echo $D->BASEURL; ?>home/login">LOGIN</a></li>
			      <li><a href="<?php echo $D->BASEURL; ?>home/fale_conosco">FALE CONOSCO</a></li>
			    </ul>
			  </section>
		</nav>
	</div>
	</div>

	<div class="small-8 small-centered large-8 columns" id="container-register">
		<div id="header"><center><span>NOVO CADASTRO</span></center></div>
		<br/>

		<form action="<?php echo $D->BASEURL; ?>home/register_process" id="form_default" name="form_default" method="post">

			<div class="row">
		    <div class="large-12 columns">
		      <label>Subdomínio
		        <input type="text" name="subdominio" id="subdominio" class="validate[required]" placeholder="Apenas o subdomínio desejado, por exemplo: corretorsp" />
		      </label>
		    </div>
		  </div>

		  <div class="row">
		    <div class="large-8 columns">
		      <label>Nome Completo
		        <input type="text" name="nome" placeholder="Nome completo" id="nome" class="validate[required]" />
		      </label>
		    </div>

		    <div class="large-4 columns">
		      <label>Data Nascimento
		        <input type="text" name="data_nascimento" placeholder="Data Nascimento" id="data_nascimento" class="validate[required]" />
		      </label>
		    </div>
		  </div>

		  <div class="row">
		    <div class="large-8 columns">
		      <label>Creci
		        <input type="text" name="creci" placeholder="Creci" />
		      </label>
		    </div>
		    <div class="large-4 columns">
		      <label>UF
		        <select name="creci_uf" id="creci_uf">
                    <option value=""></option>
                    <option value="AC">AC</option><option value="AL">AL</option><option value="AM">AM</option><option value="AP">AP</option><option value="BA">BA</option><option value="CE">CE</option><option value="DF">DF</option><option value="ES">ES</option><option value="GO">GO</option><option value="MA">MA</option><option value="MG">MG</option><option value="MS">MS</option><option value="MT">MT</option><option value="PA">PA</option><option value="PB">PB</option><option value="PE">PE</option><option value="PI">PI</option><option value="PR">PR</option><option value="RJ">RJ</option><option value="RN">RN</option><option value="RO">RO</option><option value="RR">RR</option><option value="RS" selected="">RS</option><option value="SC">SC</option><option value="SE">SE</option><option value="SP">SP</option><option value="TO">TO</option>
                </select>
		      </label>
		    </div>
		  </div>

		  <div class="row">
		    <div class="large-6 columns">
		      <label>Email
		        <input type="email" name="mail" id="email"  class="validate[required]" placeholder="Email"/>
		      </label>
		    </div>

		    <div class="large-6 columns">
		      <label>Senha
		        <input type="password" name="password" id="password"   class="validate[required]" placeholder="Senha" />
		      </label>
		    </div>
		  </div>

		  <div class="row">
		    <div class="large-6 columns">
		      <label>Estado
		      	<select name="estado">
		        <option value=""></option>
                    <option value="AC">AC</option><option value="AL">AL</option><option value="AM">AM</option><option value="AP">AP</option><option value="BA">BA</option><option value="CE">CE</option><option value="DF">DF</option><option value="ES">ES</option><option value="GO">GO</option><option value="MA">MA</option><option value="MG">MG</option><option value="MS">MS</option><option value="MT">MT</option><option value="PA">PA</option><option value="PB">PB</option><option value="PE">PE</option><option value="PI">PI</option><option value="PR">PR</option><option value="RJ">RJ</option><option value="RN">RN</option><option value="RO">RO</option><option value="RR">RR</option><option value="RS" selected="">RS</option><option value="SC">SC</option><option value="SE">SE</option><option value="SP">SP</option><option value="TO">TO</option>
                </select>
		      </label>
		    </div>

		    <div class="large-6 columns">
		      <label>Cidade
		        <input type="text" name="cidade" id="cidade" class="validate[required]" placeholder="Cidade" />
		      </label>
		    </div>
		  </div>

		  <div class="row">
		    <div class="large-12 columns">
		      <label>Bairro
		        <input type="text" name="bairro" placeholder="Bairro" />
		      </label>
		    </div>
		  </div>

		  <div class="row">
		    <div class="large-6 columns">
		      <label>Telefone Residêncial
		        <input type="text" name="phone" id="phone" class="validate[required]" placeholder="Telefone Residêncial" />
		      </label>
		    </div>

		    <div class="large-6	columns">
		      <label>Telefone Celular
		        <input type="text" name="celular" placeholder="Telefone Celular" />
		      </label>
		    </div>
		  </div>

		  <div class="row">
		    <div class="large-2 columns">
		      <label>
		        <button class="btn tiny btn-default">Criar Conta</button>
		      </label>
		    </div>
		  </div>

		 </form>
	</div>

</body>
</html>

