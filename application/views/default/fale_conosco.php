<?php
webCONFIG();
global $D;
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>MVP</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<link rel="stylesheet" href="<?php echo $D->LIBRARY; ?>foundation/css/foundation.css" />
	<link rel="stylesheet" href="<?php echo $D->CSS_PATH; ?>theme.css" />
	<script src="<?php echo $D->LIBRARY; ?>foundation/js/vendor/modernizr.js"></script>
  	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
  	<link rel="stylesheet" type="text/css" href="<?php echo $D->LIBRARY; ?>slideshow/css/demo.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $D->LIBRARY; ?>slideshow/css/style1.css" />
	<script type="text/javascript" src="<?php echo $D->LIBRARY; ?>slideshow/js/modernizr.custom.86080.js"></script>
	<style>
	select{background-color: #fff; color: #666;}
	</style>
</head>
<body>
	<ul class="cb-slideshow">
        <li><span>Image 01</span><div><h3></h3></div></li>
        <li><span>Image 02</span><div><h3></h3></div></li>
        <li><span>Image 03</span><div><h3></h3></div></li>
        <li><span>Image 04</span><div><h3></h3></div></li>
        <li><span>Image 05</span><div><h3></h3></div></li>
        <li><span>Image 06</span><div><h3></h3></div></li>
    </ul>


    <!-- content -->
	<div class="small-12 small-centered large-10 columns wide">
	<div class="nav">
		<div id="contact">
			<div id="item">305.373.0102</div>
				<div class="separator"></div>
			<div id="item"><img src="<?php echo $D->IMG_PATH; ?>email.png" width="16" alt="email contact"> email@email.com</div>
				<div class="separator"></div>
			<div id="item"><span>English</span></div>
		</div>
		<nav class="top-bar" data-topbar role="navigation">
			  <section class="top-bar-section">
			  	<ul class="center">
			      <li><a href="<?php echo $D->BASEURL; ?>">INICIO</a></li>
			      <li><a href="#">PESQUISA DE IMÓVEIS</a></li>
			      <li><a href="#">CONDOMÍNIOS</a></li>
			      <li><a href="#">MAPA DA ÁREA</a></li>
			      <li><a href="#">VÍDEOS</a></li>
			      <li><a href="#">FOTOS</a></li>
			      <li><a href="<?php echo $D->BASEURL; ?>home/register">CADASTRO</a></li>
			      <li><a href="<?php echo $D->BASEURL; ?>home/login">LOGIN</a></li>
			      <li><a href="<?php echo $D->BASEURL; ?>home/fale_conosco">FALE CONOSCO</a></li>
			    </ul>
			  </section>
		</nav>
	</div>
	</div>


	<div class="small-8 small-centered large-8 columns" id="container-register">
		<div id="header"><center><span>Fale Conosco</span></center></div>
		<br/>

		<form action="<?php echo $D->BASEURL; ?>home/fale_conosco_process" method="post">

			<div class="row">
		    <div class="large-6 columns">
		      <label>Email
		        <input type="text" name="subdominio" placeholder="Email" />
		      </label>
		    </div>

		    <div class="large-6 columns">
		      <label>Nome Completo
		        <input type="text" name="nome" placeholder="Nome completo" />
		      </label>
		    </div>
		  </div>

		  <div class="row">
		    <div class="large-12 columns">
		      <label>Mensagem
		        <textarea name="mensagem" style="height:170px;"></textarea>
		      </label>
		    </div>
		  </div>
		  <br/>

		  <div class="row">
		    <div class="large-2 columns">
		      <label>
		        <button class="btn tiny btn-default">Criar Conta</button>
		      </label>
		    </div>
		  </div>

		 </form>
	</div>

</body>
</html>
