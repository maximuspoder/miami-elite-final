<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	 <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Home</title>
	<link href="<?php PUBLIC_FOLDER_ACCESS_OUT(); ?>css/bootstrap.css" rel="stylesheet">
  <link href="<?php PUBLIC_FOLDER_ACCESS_OUT(); ?>css/estilo.css" rel="stylesheet">
    
    
	 <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="navbar-wrapper">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12">
         <header class="sobre-menu">
           <img src="<?php PUBLIC_FOLDER_ACCESS_OUT(); ?>img/logo-piquet.jpg">
             <span>english</span>
              <span class="divsors">|</span>
              <span>miami@piquetrealty.com</span>
              <span class="divsors">|</span>
              <span>305.373.0102</span>
         </header>
        </div>
      </div>
    </div>
    <div class="container">
        <nav class="navbar navbar-inverse navbar-static-top border2-red">
          
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <ul class="nav navbar-nav">
              <li class="active hidden"><a class="navbar-brand " href="#">INICIO</a></li>
              </ul>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <li class="active"> <a href="#">INICIO</a></li>
                <li><a href="#">PESQUISA DE IMÓVEIS</a></li>
                <li><a href="#about">CONDOMÍNIOS</a></li>
                <li><a href="#contact">COMERCIAL</a></li>
                <li ><a href="#">MAPA DA ÁREA </a></li>
                <li><a href="#">VÍDEOS</a></li>
                <li><a href="#">FOTOS</a></li>
                <li><a href="#">BLOGGER</a></li>
                <li><a href="#">FALE CONOSCO</a></li>
              </ul>
            </div>
          
        </nav>
        </div>
         </div>

 
<!-- ***********************************************************************************-->
  <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img src="<?php PUBLIC_FOLDER_ACCESS_OUT(); ?>img/bg4.jpg"  alt="First slide">
          <div class="container">
            <div class="carousel-caption">
              <h1>LOrem ipsum.</h1>
              <p>LOrem ipsum, LOrem ipsum, LOrem ipsum.</p>
            </div>
          </div>
        </div>
        <div class="item">
          <img src="<?php PUBLIC_FOLDER_ACCESS_OUT(); ?>img/bg3.jpg"  alt="Second slide">
          <div class="container">
            <div class="carousel-caption">
              <h1>LOrem ipsum.</h1>
              <p>LOrem ipsum, LOrem ipsum, LOrem ipsum.</p>
            </div>
          </div>
        </div>
        <div class="item">
          <img src="<?php PUBLIC_FOLDER_ACCESS_OUT(); ?>img/bg2.jpg" alt="Third slide">
          <div class="container">
            <div class="carousel-caption">
              <h1>LOrem ipsum.</h1>
              <p>LOrem ipsum, LOrem ipsum, LOrem ipsum.</p>
            </div>
          </div>
        </div>
      </div>
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div><!-- /.carousel -->
 
    <!-- ***********************************************************************************-->

  <div class="container">
  <div class="row">
   <div class="col-md-12 col-lg-12">
   <ul class="nav nav-tabs" id="myTab">
    <li class="active" ><a href="#home" data-toggle="pill">PESQUISA RÁPIDA</a></li>
    <li><a href="#profile" data-toggle="pill">PESQUISA COMPLETA</a></li>
    <li><a href="#messages" data-toggle="pill">PESQUISA AVANÇADA</a></li>
  </ul>
  <div class="tab-content">
  <div class="tab-pane active" id="home"><section style="height:700px;background-color:#2D2F3B;"><h1>Aba1</h1></section></div>
  <div class="tab-pane" id="profile"><section style="height:700px;background-color:#2D2F3B;"><h1>Aba2</h1></section></div>
  <div class="tab-pane" id="messages"><section style="height:700px;background-color:#2D2F3B;"><h1>Aba3</h1></section></div>

</div>
   </div>
     </div>
 <div class="col-md-12 col-lg-12 ">
       <div class="row row-title-blog">   
 <div class="col-md-12 col-lg-12 ">

       <header class="title-section-blog"> ESCOLHA A SUA ÁREA</header>

</div>     </div></div>
<div class="col-md-12 col-lg-12 separete-sections">
<div class="row">
 <section class="section-article-images">

   <div class="col-md-4 col-lg-4">
    <article class="article-images">
       <img src="<?php PUBLIC_FOLDER_ACCESS_OUT(); ?>img/thumb-miami.jpg"  alt="foto mimami">
       <h2>MIAMI</h2>
       <p>
         Lorem ipsum dolor sit amet, consectetur adipiscing elit. nec. Sed in ligula mattis, volutpat mi et, aliquam magna. 
      
       </p>
       <a class="glyphicon glyphicon-play-circle"aria-hidden="true" href="#"></a>
</article>
   </div>
    <div class="col-md-4 col-lg-4">
  <article class="article-images">
       <img src="<?php PUBLIC_FOLDER_ACCESS_OUT(); ?>img/thumb-orlando.jpg"  alt="foto orlando">
       <h2>Orlando</h2>
       <p>
         Lorem ipsum dolor sit amet, consectetur adipiscing elit. nec. Sed in ligula mattis, volutpat mi et, aliquam magna. 
      
       </p>
       <a class="glyphicon glyphicon-play-circle"aria-hidden="true" href="#"></a>
</article>
    </div>
     <div class="col-md-4 col-lg-4">
      <article class="article-images">
       <img src="<?php PUBLIC_FOLDER_ACCESS_OUT(); ?>img/thumb-new-york.jpg" alt="foto new york">
       <h2>New York</h2>
       <p>
         Lorem ipsum dolor sit amet, consectetur adipiscing elit. nec. Sed in ligula mattis, volutpat mi et, aliquam magna.
       </p>
       <a class="glyphicon glyphicon-play-circle"aria-hidden="true" href="#"></a>
</article>
     </div>
 </section>
</div>
</div>
       
 <div class="col-md-12 col-lg-12 ">
       <div class="row row-title-blog">   
 <div class="col-md-12 col-lg-12 ">

       <header class="title-section-blog"> IMÓVEIS BLOG</header>

</div>     </div></div>
<div class="col-md-12 col-lg-12">
<div class="row">
 <section class="section-imoveis-blog">

   <div class="col-md-4 col-lg-4">
    <article class="article-posts">
       <h2>Imóveis em William Islands</h2>
       <p>
         Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus tempus ipsum est, et pellentesque tellus gravida nec. Sed in ligula mattis, volutpat mi et, aliquam magna. 
         Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus tempus ipsum est, et pellentesque tellus gravida nec. Sed in ligula mattis, volutpat mi et, aliquam magna.  amet eget dui.
       </p>
       <a class="glyphicon glyphicon-play-circle"aria-hidden="true" href="#"></a>
</article>
   </div>
    <div class="col-md-4 col-lg-4">
      <article class="article-posts">
      <h2>Imóveis em William Islands</h2>
       <p>
         Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus tempus ipsum est, et pellentesque tellus gravida nec. Sed in ligula mattis, volutpat mi et, aliquam magna.
         Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus tempus ipsum est, et pellentesque tellus gravida nec. Sed in ligula mattis, volutpat mi et, aliquam magna.   amet eget dui.
       </p>
       <a class="glyphicon glyphicon-play-circle"aria-hidden="true" href="#"></a>
    </article>
    </div>
     <div class="col-md-4 col-lg-4">
    <article class="article-posts">
       <h2>Imóveis em William Islands</h2>
       <p>
         Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus tempus ipsum est, et pellentesque tellus gravida nec. Sed in ligula mattis, volutpat mi et, aliquam magna.
         Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus tempus ipsum est, et pellentesque tellus gravida nec. Sed in ligula mattis, volutpat mi et, aliquam magna.   amet eget dui.
       </p>
       <a class="glyphicon glyphicon-play-circle"aria-hidden="true" href="#"></a>
     </article>
     </div>
 </section>
</div>
</div>

<div class="col-md-12 col-lg-12">
<div class="row">
  <footer  class="text-color-site">   
<div class="col-md-2 col-lg-2 border-footer" >

   <ul class="lista-footer ">
     <li><a href="#">INICIO</a></li>
     <li><a href="#">PESQUISA DE IMÓVEIS</a></li>
     <li><a href="#">CONDOMÍNIOS</a></li>
     <li><a href="#">COMERCIAL</a></li>
     <li><a href="#">MAPA DA ÁREA</a></li>
     <li><a href="#">VÍDEOS</a></li>
     <li><a href="#">FOTOS</a></li>
     <li><a href="#">BLOG </a></li>
     <li><a href="#">FALE CONOSCO</a></li>
   </ul>
  
</div>
<div  class="col-md-3 col-lg-3 border-footer" >

  <ul class="lista-footer">
     <li><a href="#">TRADIÇÃO AUTOMOBILISTICA</a></li>
     <li><a href="#">ALCANCE GLOBAL</a></li>
     <li><a href="#">EXPERIÊNCIA GLOBAL</a></li>
     <li><a href="#">OPORTUNIDADE DE CARREIRA</a></li>
     <li><a href="#">CRISTIANO PIQUET</a></li>
     <li><a href="#">EMPRESAS AFILIADAS</a></li>
   </ul>
 
</div>
<div class="col-md-3 col-lg-3 border-footer">

<h4>Veja em noso site casas e apartamentos exclusivos
 Os melhores e mais luxuosos imóveis em Miami</h4>
  <ul class="lista-footer pad-left">
    <li class="glyphicon glyphicon-play-circle" aria-hidden="true"><a href="#">Imóveis em Miami</a></li>
     <li class="glyphicon glyphicon-play-circle" aria-hidden="true"><a href="#"> Casas em Miami</a></li>
    
     <li class="glyphicon glyphicon-play-circle"aria-hidden="true"><a href="#">Apartamentos em Miami</a></li>
     <li class="glyphicon glyphicon-play-circle"aria-hidden="true"><a href="#">Propriedades em Miami</a></li>
     <li class="glyphicon glyphicon-play-circle"aria-hidden="true"><a href="#">Investir em Miami</a></li>
   </ul>
  
  
</div>
<div  class="col-md-4 col-lg-4  no-pad-right">
 <section class="section-left-footer">
  <figure class="footer-figure" >
   <img  class="footer-figure-img"src="<?php PUBLIC_FOLDER_ACCESS_OUT(); ?>img/logo-piquet.jpg">
   </figure>
   <address>
   <p class="direitos"> <span class="glyphicon glyphicon-copyright-mark" aria-hidden="true"></span> 2014 PIQUET REALITY<br>
   Todos os direitos reservados</p>
 <p> miami@piquetreality.com</p>
    <P>(55) 11 3230 - 1863 Sao Paulo</p>
    <P>(55) 21 3230 - 1863 Rio de Janeiro</p> 
    <P>(55) 31 3230 - 1863 Belo Horizonte</p>
    <P>(55) 61 3230 - 1863 Brasilia</p>


    <address>
 </section>
  <section  class="section-right-footer">
          <div class="footer-boxes">
            <h5>Orlando</h5>
            <p>9950 condoy Winderley Road</p>
            <p>Widerlei FL 34786</p>
          </div>
          <div class="footer-boxes"> 
            <h5>Miami</h5>
            <p>9950 condoy Miami Road</p>
            <p>Miami FL 34786</p>
            <p>(305) 3730102</p>
          </div>
          <div class="footer-boxes">
             <h5>New York</h5>
            <p>9950 condoy New York Road</p>
            <p>New York FL 34786</p>
          </div>
 </section>
</div>

 </footer> 
</div> 
  </div> 
</div>

 <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
 <!-- Include all compiled plugins (below), or include individual files as needed -->
 <script src="<?php PUBLIC_FOLDER_ACCESS_OUT(); ?>js/bootstrap.min.js"></script>

</body>
</html>