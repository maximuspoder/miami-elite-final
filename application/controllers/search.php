<?php
/**
 * User: fernandoalls
 * Date: 02/02/15
 * Time: 19:26
 */
class Search extends CI_Controller{


	public static $districtMap = array(
		'North Miami' => 'NMIAMI',
		'Miami' => 'MIAMI',
		'North Miami Beach' => 'NMIAMIBC',
		'Fort Lauderdale' => 'FORTLAUD',
		'Pembroke Pines' => 'PEMBPINE',
		'Coral Gables' => 'CORALGBL',
		'Miami Lakes' => 'MIAMILKE',
		'Hollywood' => 'HOLLYWD',
		'Miramar' => 'MIRAMAR',
		'Pompano Beach' => 'POMPANO',
		'Pinecrest' => 'PINECRST',
		'Miami Shores' => 'MIAMISHR',
		'Lauderhill' => 'LAUDHILL',
		'Miami Gardens' => 'MIAMIGAR',
		'Weston' => 'WESTON',
		'Opa-locka' => 'OPALOCKA',
		'Homestead' => 'HOMESTED',
		'Homestead' => 'DADECNTY',
		'Miami Beach' => 'MIAMIBCH',
		'OTHERCTY' => 'OTHERCTY',
		'Plantation' => 'PLANTATN',
		'Key Biscayne' => 'KEYBISCY',
		'Dania Beach' => 'DANIABCH',
		'OTHERISL' => 'OTHERISL',
		'Tamarac' => 'TAMARAC',
		'ROYAL PB' => 'ROYALPB',
		'Cooper City' => 'COOPERCI',
		'Palm Bay' => 'PALMEBAY',
		'Hallandale Beach' => 'HALLNDLE',
		'Davie' => 'DAVIE',
		'Boynton Beach' => 'BOYNTON',
		'Hialeah' => 'HIALEAH',
		'North Lauderdale' => 'NLAUDER',
		'OTHERFLA' => 'OTHERFLA',
		'Surfside' => 'SURFSIDE',
		'Doral' => 'DORAL',
		'Fort Pierce' => 'FTPIERCE',
		'Miami Springs' => 'MIAMISPR',
		'Coral Springs' => 'CORALSPR',
		'Coconut Grove' => 'COCOGROV',
		'Cutler Bay' => 'CUTLRBAY',
		'Parkland' => 'PARKLAND',
		'Boca Raton' => 'BOCA',
		'Wellington' => 'WELLINGT',
		'Sunny Isles Beach' => 'SUNNYISL',
		'Aventura' => 'AVENTUR',
		'Palm Springs' => 'PALMSPRG',
		'ISLCARBN' => 'ISLCARBN',
		'DELRAY' => 'DELRAY',
		'Coconut Creek' => 'COCOCRK',
		'South Miami' => 'SMIAMI',
		'Hialeah Gardens' => 'HIALGRDN',
		'Margate' => 'MARGATE',
		'West Park' => 'WSTPRK',
		'OTHER' => 'OTHER',
		'Sunrise' => 'SUNRISE',
		'Loxahatchee' => 'LOXAHAT',
		'Naranja' => 'NARANJA',
		'Golden Beach' => 'GOLDNBCH',
		'Pembroke Park' => 'PEMBPARK',
		'Kendall' => 'KENDALL',
		'Lake Worth' => 'LAKEWORT',
		'Deerfield Beach' => 'DEERFLD',
		'Biscayne Park' => 'BISCPARK',
		'Lauderdale-By-The-Sea' => 'LAUDBSEA',
		'Southwest Ranches' => 'SWRANCH',
		'Port St Lucie' => 'PTSTLUCE',
		'Bay Harbor Islands' => 'BAYHARIS',
		'Oakland Park' => 'OAKPARK',
		'Riviera Beach' => 'RIVIERA',
		'Lauderdale Lakes' => 'LAUDLAKE',
		'Florida City' => 'FLACITY',
		'VIRGRDNS' => 'VIRGRDNS',
		'SWEETWTR' => 'SWEETWTR',
		'Wilton Manors' => 'WILTONMN',
		'El Portal' => 'ELPORTAL',
		'OTHERUSA' => 'OTHERUSA',
		'Lighthouse Point' => 'LHPOINT',
		'BISGRDNS' => 'BISGRDNS',
		'Lantana' => 'LANTANA',
		'Highland' => 'HIGHLAND',
		'Palm Beach Gardens' => 'PBGARDEN',
		'North Bay Village' => 'NBAYVLGE',
		'FISHISLD' => 'FISHISLD',
		'BALHARBR' => 'BALHARBR',
		'Greenacres' => 'GREENACR',
		'Palm Beach' => 'PALMBEAC',
		'EASTERNS' => 'EASTERNS',
		'Hillsboro Beach' => 'HILLSBRO',
		'Jupiter' => 'JUPITER',
		'HUTCHNSN' => 'HUTCHNSN',
		'BELLEGLA' => 'Belle Glade',
	);


	public function __construct(){
		parent::__construct();
		$this->load->model('acesso_externo_model', 'acesso_externo');
		$owner = $this->acesso_externo->getInfo();
		$OwnerInfo = array(
			'owner' => true,
			'userid' => $owner[0]->userid,
			'total' => $owner[0]->total,
			'subdominio' => $owner[0]->subdominio,
			'nome_completo' => $owner[0]->nome_completo,
			'creci' => $owner[0]->creci,
			'email' => $owner[0]->email,
			'telefone' => $owner[0]->telefone,
			'profile_picture' => $owner[0]->profile_picture
		);
		if($owner[0]->id != ""){
			$this->session->set_userdata($OwnerInfo);	
		} else {
			header('location: http://miamielite.com.br');
		}
	}

	public function index(){

		if($this->session->userdata('profile_picture') != ""){
			$args['foto_perfil'] = PROFILEPICTURELINK .'profile_'.$this->session->userdata('userid').'/'.$this->session->userdata('profile_picture');	
		} else {
			$args['foto_perfil'] = PROFILEPICTURELINK .'default.jpg';	
		}

		$args['imoveis'] = $this->acesso_externo->getImoveis();


		$params = new stdClass();
		$params->mlnumber 	= trim($this->input->post('mlnumber'));
		$params->price 		= trim($this->input->post('price'));
		$params->zipcode 	= trim($this->input->post('zipcode'));
		$params->city 		= trim($this->input->post('city'));

		$args['search_params'] = $this->input->post();

		#print_r($this->input->post()); die();

		$args['property1'] = $this->acesso_externo->getResultByTableProperty('property1', $params, self::$districtMap);
		$args['property2'] = $this->acesso_externo->getResultByTableProperty('property2', $params, self::$districtMap);
		$args['property3'] = $this->acesso_externo->getResultByTableProperty('property3', $params, self::$districtMap);
		$args['property4'] = $this->acesso_externo->getResultByTableProperty('property4', $params, self::$districtMap);
		$args['property5'] = $this->acesso_externo->getResultByTableProperty('property5', $params, self::$districtMap);
		$args['property6'] = $this->acesso_externo->getResultByTableProperty('property6', $params, self::$districtMap);
		$args['property7'] = $this->acesso_externo->getResultByTableProperty('property7', $params, self::$districtMap);
		$args['property8'] = $this->acesso_externo->getResultByTableProperty('property8', $params, self::$districtMap);

		$args['merge'] = array_merge($args['property1'], $args['property2'],
									$args['property3'], $args['property4'],
									$args['property5'], $args['property6'],
									$args['property7'], $args['property8']);

		#print_r($args['merge']);
		VIEW('/home/search', $args);
	}

}