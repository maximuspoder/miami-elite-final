<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax_dashboard extends CI_Controller {

	function saveItem(){
		if($this->session->userdata('logado') == true){
			$args['data'] = $this->input->post();

			//print_r($args['data']); die();

			$arrParam['subdominio'] = $this->session->userdata('subdominio');
			$arrParam['sysid'] 		= getValue($args['data'], 'sysid');
			$arrParam['field_61'] 	= getValue($args['data'], 'county');
			$arrParam['field_157'] 	= getValue($args['data'], 'ml');
			$arrParam['field_922'] 	= getValue($args['data'], 'city');
			$arrParam['field_25'] 	= getValue($args['data'], 'bed');
			$arrParam['field_19'] 	= getValue($args['data'], 'area');
			$arrParam['field_10'] 	= getValue($args['data'], 'zipcode');
			$arrParam['field_247'] 	= getValue($args['data'], 'street');
			$arrParam['field_248'] 	= getValue($args['data'], 'number');
			$arrParam['field_191'] 	= getValue($args['data'], 'pool');
			$arrParam['field_1339'] = getValue($args['data'], 'description');
			$arrParam['field_137'] 	= getValue($args['data'], 'price');
			$arrParam['field_1140'] = getValue($args['data'], 'ml');
			$arrParam['field_1412'] = getValue($args['data'], 'city');
			$arrParam['field_1152'] = getValue($args['data'], 'bed');
			$arrParam['field_1149'] = getValue($args['data'], 'area');
			$arrParam['field_1153'] = getValue($args['data'], 'zipcode');
			$arrParam['field_1148'] = getValue($args['data'], 'street');
			$arrParam['field_1146'] = getValue($args['data'], 'number');
			$arrParam['field_1145'] = getValue($args['data'], 'price');

			$this->system_model->insert('imoveis', $arrParam);

			if($this->system_model->affected_rows() > 0){
				echo 1;
			} else {
				echo 0;
			}

		}
	}
}