<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * @description Controller responsable for search properties 
 * @author Fernando Alves
 * @package controllers
 * @subpackage controllers.dashboard
 *
 * Common required fields in Properties
 * 157 => ML Number
 * 10 => Zip Code
 *
 *
 *
 *
 */
class Dashboard extends CI_Controller {


	public static $districtMap = array(
		'North Miami' => 'NMIAMI',
		'Miami' => 'MIAMI',
		'North Miami Beach' => 'NMIAMIBC',
		'Fort Lauderdale' => 'FORTLAUD',
		'Pembroke Pines' => 'PEMBPINE',
		'Coral Gables' => 'CORALGBL',
		'Miami Lakes' => 'MIAMILKE',
		'Hollywood' => 'HOLLYWD',
		'Miramar' => 'MIRAMAR',
		'Pompano Beach' => 'POMPANO',
		'Pinecrest' => 'PINECRST',
		'Miami Shores' => 'MIAMISHR',
		'Lauderhill' => 'LAUDHILL',
		'Miami Gardens' => 'MIAMIGAR',
		'Weston' => 'WESTON',
		'Opa-locka' => 'OPALOCKA',
		'Homestead' => 'HOMESTED',
		'Homestead' => 'DADECNTY',
		'Miami Beach' => 'MIAMIBCH',
		'OTHERCTY' => 'OTHERCTY',
		'Plantation' => 'PLANTATN',
		'Key Biscayne' => 'KEYBISCY',
		'Dania Beach' => 'DANIABCH',
		'OTHERISL' => 'OTHERISL',
		'Tamarac' => 'TAMARAC',
		'ROYAL PB' => 'ROYALPB',
		'Cooper City' => 'COOPERCI',
		'Palm Bay' => 'PALMEBAY',
		'Hallandale Beach' => 'HALLNDLE',
		'Davie' => 'DAVIE',
		'Boynton Beach' => 'BOYNTON',
		'Hialeah' => 'HIALEAH',
		'North Lauderdale' => 'NLAUDER',
		'OTHERFLA' => 'OTHERFLA',
		'Surfside' => 'SURFSIDE',
		'Doral' => 'DORAL',
		'Fort Pierce' => 'FTPIERCE',
		'Miami Springs' => 'MIAMISPR',
		'Coral Springs' => 'CORALSPR',
		'Coconut Grove' => 'COCOGROV',
		'Cutler Bay' => 'CUTLRBAY',
		'Parkland' => 'PARKLAND',
		'Boca Raton' => 'BOCA',
		'Wellington' => 'WELLINGT',
		'Sunny Isles Beach' => 'SUNNYISL',
		'Aventura' => 'AVENTUR',
		'Palm Springs' => 'PALMSPRG',
		'ISLCARBN' => 'ISLCARBN',
		'DELRAY' => 'DELRAY',
		'Coconut Creek' => 'COCOCRK',
		'South Miami' => 'SMIAMI',
		'Hialeah Gardens' => 'HIALGRDN',
		'Margate' => 'MARGATE',
		'West Park' => 'WSTPRK',
		'OTHER' => 'OTHER',
		'Sunrise' => 'SUNRISE',
		'Loxahatchee' => 'LOXAHAT',
		'Naranja' => 'NARANJA',
		'Golden Beach' => 'GOLDNBCH',
		'Pembroke Park' => 'PEMBPARK',
		'Kendall' => 'KENDALL',
		'Lake Worth' => 'LAKEWORT',
		'Deerfield Beach' => 'DEERFLD',
		'Biscayne Park' => 'BISCPARK',
		'Lauderdale-By-The-Sea' => 'LAUDBSEA',
		'Southwest Ranches' => 'SWRANCH',
		'Port St Lucie' => 'PTSTLUCE',
		'Bay Harbor Islands' => 'BAYHARIS',
		'Oakland Park' => 'OAKPARK',
		'Riviera Beach' => 'RIVIERA',
		'Lauderdale Lakes' => 'LAUDLAKE',
		'Florida City' => 'FLACITY',
		'VIRGRDNS' => 'VIRGRDNS',
		'SWEETWTR' => 'SWEETWTR',
		'Wilton Manors' => 'WILTONMN',
		'El Portal' => 'ELPORTAL',
		'OTHERUSA' => 'OTHERUSA',
		'Lighthouse Point' => 'LHPOINT',
		'BISGRDNS' => 'BISGRDNS',
		'Lantana' => 'LANTANA',
		'Highland' => 'HIGHLAND',
		'Palm Beach Gardens' => 'PBGARDEN',
		'North Bay Village' => 'NBAYVLGE',
		'FISHISLD' => 'FISHISLD',
		'BALHARBR' => 'BALHARBR',
		'Greenacres' => 'GREENACR',
		'Palm Beach' => 'PALMBEAC',
		'EASTERNS' => 'EASTERNS',
		'Hillsboro Beach' => 'HILLSBRO',
		'Jupiter' => 'JUPITER',
		'HUTCHNSN' => 'HUTCHNSN',
		'BELLEGLA' => 'Belle Glade',
	);

	/**
	 * @description Load constructor
	 * @return void
	 */
	public function __construct(){
		parent::__construct();
		$this->load->model('rets_model', 'rets');
		$this->load->model('dashboard_model', 'dashboard');
		$this->load->model('user_model', 'user');
		$this->load->model('system_model', 'system');
	}

	/**
	 * @description Load home page from dashboard
	 * @return void
	 */
	public function index(){
		if($this->session->userdata('logado') == true){
			foreach(self::$districtMap as $key =>$value){
				$args['cityname'][] = $key;
			}

			VIEW('dashboard/default', $args);
		} else {
			redirect();
		}
	}

	/**
	 * @description Execute search for rets
	 * @return void
	 */
	public function search(){
		if($this->session->userdata('logado') == true){
			// Create new stdClass to pass like object to model
			$params = new stdClass();
			$params->mlnumber = $this->input->post('mlnumber');
			$params->quantity = $this->input->post('quantity');
			$params->zipcode = $this->input->post('zipcode');
			$params->cityname = $this->input->post('cityname');
			$Cities = explode(',', $params->cityname);
			array_pop($Cities);
			$params->cities = $Cities;
			// Set limit to search in each table
			$params->quantity = ($params->quantity != 'null') ? ceil($params->quantity / 8) : 0;
			// A1586293 | M1220482
			$args['property1'] = $this->rets->getResultByTableProperty('property1', $params, self::$districtMap, $this->session->userdata('id'));
			$args['property2'] = $this->rets->getResultByTableProperty('property2', $params, self::$districtMap, $this->session->userdata('id'));
			$args['property3'] = $this->rets->getResultByTableProperty('property3', $params, self::$districtMap, $this->session->userdata('id'));
			$args['property4'] = $this->rets->getResultByTableProperty('property4', $params, self::$districtMap, $this->session->userdata('id'));
			$args['property5'] = $this->rets->getResultByTableProperty('property5', $params, self::$districtMap, $this->session->userdata('id'));
			$args['property6'] = $this->rets->getResultByTableProperty('property6', $params, self::$districtMap, $this->session->userdata('id'));
			$args['property7'] = $this->rets->getResultByTableProperty('property7', $params, self::$districtMap, $this->session->userdata('id'));
			$args['property8'] = $this->rets->getResultByTableProperty('property8', $params, self::$districtMap, $this->session->userdata('id'));


			$args['params'] = $params;
			$args['cities'] = $this->input->post('cityname');
			$args['districtMap'] = self::$districtMap;

			VIEW('dashboard/search', $args);
		} else {
			redirect();
		}
	}

	public function featured(){
		if($this->session->userdata('logado') == true && $this->session->userdata('tipousuario') == 1){
			$this->load->model('system_model', 'system');
			$args['featured'] = $this->dashboard->getFeatured();
			VIEW('dashboard/featured', $args);
		} else {
			redirect();
		}
	}

	public function featured_new(){

		foreach(self::$districtMap as $key =>$value){
			$args['cityname'][] = $key;
		}

		if($this->session->userdata('logado') == true && $this->session->userdata('tipousuario') == 1){
			VIEW('dashboard/featured_new', $args);
		} else {
			redirect();
		}
	}

	public function featured_new_process(){
		if($this->session->userdata('logado') == true && $this->session->userdata('tipousuario') == 1){

			//print_r($this->input->post()); die();
			$arr['cidade'] 	= $this->input->post('cidade');
			$arr['banheiros'] 	= $this->input->post('banheiros');
			$arr['quartos']	= $this->input->post('quartos');
			$arr['descricao']	= $this->input->post('descricao');
			$arr['created_at']	= date('Y-m-d H:i:s');
			$arr['updated_at']	=  date('Y-m-d H:i:s');
			
			// Insert into
			$this->system->insert('featured', $arr);

			if($this->system->affected_rows() > 0){
				redirect('dashboard/featured_upload/'. $this->system->last_id());
			} else {
				redirect('dashboard/featured_new');
			}
		} else {
			redirect();
		}
	}

	public function featured_delete($id){
		if($this->session->userdata('logado') == true && $this->session->userdata('tipousuario') == 1){
			$where = array('id' => $id);
			$this->system->delete('featured', $where);
			if($this->system->affected_rows() > 0){
				redirect('dashboard/featured');
			} else {
				redirect('dashboard/featured');
			}
		} else {
			redirect();
		}
	}

	public function featured_upload($uploadid){
		if($this->session->userdata('logado') == true && $this->session->userdata('tipousuario') == 1){
			$args['uploadid'] = $uploadid;
			VIEW('dashboard/featured_upload', $args);
		} else {
			redirect();
		}
	}

	public function featured_upload_process($id){

		

		$tempFile = $_FILES['file']['tmp_name'];
		$format = end(explode('.', $_FILES['file']['name']));
		$photo_name = md5( date('Y-m-d H:i:s') . $_FILES['file']['tmp_name'] ) . '.' . $format;


		$DIR = FEATURED . 'featured_'.$id.'/';

		if (!file_exists($DIR . $photo_name)) {
		    mkdir($DIR, 0777);		    
		}

		$arrParam['iddestaque'] = $id;
		$arrParam['foto'] = $photo_name;

		if(move_uploaded_file($tempFile, $DIR . $photo_name)){
			$this->load->model('system_model', 'system');
			$this->system->insert('featured_photos', $arrParam);
			if($this->system->affected_rows() > 0){
				echo 1;
			} else {
				echo 0;
			}
		} else {
			echo $DIR . $photo_name;
		}
	}



	public function corretores(){
		if($this->session->userdata('logado') == true && $this->session->userdata('tipousuario') == 1){
			
			$args['corretores'] = $this->dashboard->getCorretores($this->session->userdata('id'));
			VIEW('dashboard/corretores', $args);
		} else {
			redirect();
		}
	}

	public function corretor_delete($id){
		if($this->session->userdata('logado') == true && $this->session->userdata('tipousuario') == 1){
			
			$where = array('id' => $id);
			$this->system->delete('users', $where);
			if($this->system->affected_rows() > 0){
				redirect('dashboard/corretores');
			} else {
				redirect('dashboard/corretores');
			}
		} else {
			redirect();
		}
	}

	public function corretor_imoveis($userid){
		if($this->session->userdata('logado') == true && $this->session->userdata('tipousuario') == 1){
			

			$args['store1'] = $this->dashboard->get_userStoreByID('property1', 'Property1', $userid);
			$args['store2'] = $this->dashboard->get_userStoreByID('property2', 'Property2', $userid);
			$args['store3'] = $this->dashboard->get_userStoreByID('property3', 'Property3', $userid);
			$args['store4'] = $this->dashboard->get_userStoreByID('property4', 'Property4', $userid);
			$args['store5'] = $this->dashboard->get_userStoreByID('property5', 'Property5', $userid);
			$args['store6'] = $this->dashboard->get_userStoreByID('property6', 'Property6', $userid);
			$args['store7'] = $this->dashboard->get_userStoreByID('property7', 'Property7', $userid);
			$args['store8'] = $this->dashboard->get_userStoreByID('property8', 'Property8', $userid);

			$args['districtMap'] = self::$districtMap;
			VIEW('dashboard/corretor_imoveis', $args);
		} else {
			redirect();
		}
	}

	/**
	 * @description Show single property
	 * @return void
	 */
	public function showProperty($getProperty, $getSysid){
		if($this->session->userdata('logado') == true){

			$sysid = intval($getSysid);
			$property = intval($getProperty);
			$args['result'] = $this->rets->getPropertyBySYSID($property, $sysid);
			$args['sysid'] = $sysid;

			if($property == 1){ $args['map'] = $this->rets->map['property1']; }
			elseif($property == 2){ $args['map'] = $this->rets->map['property2']; }
			elseif($property == 3){ $args['map'] = $this->rets->map['property3']; }
			elseif($property == 4){ $args['map'] = $this->rets->map['property4']; }
			elseif($property == 5){ $args['map'] = $this->rets->map['property5']; }
			elseif($property == 6){ $args['map'] = $this->rets->map['property6']; }
			elseif($property == 7){ $args['map'] = $this->rets->map['property7']; }
			elseif($property == 8){ $args['map'] = $this->rets->map['property8']; }

			VIEW('dashboard/showProperty', $args);
		} else {
			redirect();
		}
	}

	/**
	 * @description Show user properties
	 * @return void
	 */
	function meusImoveis(){
		if($this->session->userdata('logado') == true){
			$args['resultArray'] = $this->system_model->select_where('imoveis', ' subdominio = "'. $this->session->userdata('subdominio').'"');

			VIEW('dashboard/meus_imoveis', $args);
		} else {
			redirect();
		}
	}

	/**
	 * @description Show user properties
	 * @return void
	 */
	function userStore(){
		if($this->session->userdata('logado') == true){
			// Get all items from user
			$args['store1'] = $this->dashboard->get_userStore('property1', 'Property1');
			$args['store2'] = $this->dashboard->get_userStore('property2', 'Property2');
			$args['store3'] = $this->dashboard->get_userStore('property3', 'Property3');
			$args['store4'] = $this->dashboard->get_userStore('property4', 'Property4');
			$args['store5'] = $this->dashboard->get_userStore('property5', 'Property5');
			$args['store6'] = $this->dashboard->get_userStore('property6', 'Property6');
			$args['store7'] = $this->dashboard->get_userStore('property7', 'Property7');
			$args['store8'] = $this->dashboard->get_userStore('property8', 'Property8');

			$args['districtMap'] = self::$districtMap;
			VIEW('dashboard/userStore', $args);
		} else {
			redirect();
		}
	}

	/**
	 * @description Show user data
	 * @return void
	 */
	function userInformation($updated = null){
		if($this->session->userdata('logado') == true){
			$args['user'] = $this->dashboard->userInformation($this->session->userdata('id'));
			$args['updated'] = $updated;
			if($updated == 1){
				$args['info'] = 'Sucesso!';
				$args['message'] = 'Seus dados foram alterados com sucesso.';
			} 
			else if($updated == 0){
				$args['info'] = 'Algo saiu errado!';
				$args['message'] = 'Seus dados não foram alterados, tente novamente.';
			}
			
			VIEW('dashboard/userInformation', $args);
		} else {
			redirect();
		}
	}

	function userInformation_upload($id = null){
		$tempFile = $_FILES['file']['tmp_name'];
		$format = end(explode('.', $_FILES['file']['name']));
		$photo_name = md5( date('Y-m-d H:i:s') . $_FILES['file']['tmp_name'] ) . '.' . $format;


		$DIR = PROFILEPICTURE . 'profile_'.$id.'/';

		if (!file_exists($DIR . $photo_name)) {
		    mkdir($DIR, 0777);		    
		}

		if(move_uploaded_file($tempFile, $DIR . $photo_name)){
			$this->load->model('system_model', 'system');

			$arrParam['profile_picture'] = $photo_name;
			$where = array('id' => $id);
			$this->system->update('users', $arrParam, $where);
			if($this->system->affected_rows() > 0){
				echo 1;
			} else {
				echo 0;
			}
		} else {
			echo $DIR . $photo_name;
		}
	}	

	/**
	 * @description Show user data
	 * @return void
	 */
	function userInformation_process(){
		if($this->session->userdata('logado') == true){
			$this->user->setNome($this->input->post('novo_nome'));
			$this->user->setEmail($this->input->post('novo_email'));
			if($this->input->post('nova_senha') != ""){
				$this->user->setSenha($this->input->post('nova_senha'));
			}
			$this->user->setCreci($this->input->post('novo_creci'));
			$this->user->setTelefone($this->input->post('novo_telefone'));

			$updateSenha = ($this->user->getSenha() == "") ? 0 : 1;

			$callback = $this->user->update($updateSenha, $this->session->userdata('id'));

			if($callback == 1){
				redirect('dashboard/userInformation/1');
			}
			else if($callback == 0){
				redirect('dashboard/userInformation/0');	
			}

		} else {
			redirect();
		}
	}


	/**
	 * @description Set property to user
	 * @return int
	 */
	public function ajax_set_property(){
		if($this->session->userdata('logado') == true){

			$params = new stdClass();
			$params->mlnumber 	= $this->input->post('mlnumber');
			$params->userid 	= intval($this->input->post('id'));

			if($params->mlnumber != null){
				$affected = $this->dashboard->ajax_set_property($params);
				echo $affected;
			} else {
				echo 0;
			}
		} else {
			redirect();
		}
	}

	/**
	 * @description Remove property of user
	 * @return int
	 */
	public function ajax_remove_property(){
		if($this->session->userdata('logado') == true){

			$params = new stdClass();
			$params->mlnumber 	= $this->input->post('mlnumber');
			$params->userid 	= intval($this->input->post('id'));

			if($params->mlnumber != null){
				$affected = $this->dashboard->ajax_remove_property($params);
				echo $affected;
			} else {
				echo 0;
			}
		} else {
			redirect();
		}
	}

}#end class