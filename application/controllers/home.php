<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public $domain = null;

	public function __construct(){
		#ob_start();
		parent::__construct();
		$this->load->model('acesso_externo_model', 'externo');
		// Set sessions with subdomain info
		
			$owner = $this->externo->getInfo();
			$OwnerInfo = array(
				'owner' => true,
				'userid' => $owner[0]->userid,
				'total' => $owner[0]->total,
				'subdominio' => $owner[0]->subdominio,
				'nome_completo' => $owner[0]->nome_completo,
				'creci' => $owner[0]->creci,
				'email' => $owner[0]->email,
				'telefone' => $owner[0]->telefone,
				'profile_picture' => $owner[0]->profile_picture
			);

			if($owner[0]->id != ""){
				$this->session->set_userdata($OwnerInfo);	
			} else {
				header('location: http://miamielite.com.br');
			}
	}

	/*
	* @method projeto()
	* carrega projeto no subdominio
	*/
	public function register(){
		VIEW('home/register');
	}

	public function index(){

		if($this->session->userdata('profile_picture') != ""){
			$args['foto_perfil'] = PROFILEPICTURELINK .'profile_'.$this->session->userdata('userid').'/'.$this->session->userdata('profile_picture');	
		} else {
			$args['foto_perfil'] = PROFILEPICTURELINK .'default.jpg';	
		}
		$args['imoveis'] = $this->externo->getImoveis();

		//print_r($args['imoveis']); die();
		VIEW('home/home', $args);
	}


	/*
	* @method register_process()
	* processa cadastro
	*/
	public function doRegister(){
		if(POST()){

			$args['form'] = POST();
			$this->load->model('user_model');

			// method @setters
			$this->user_model->setNome( getValue($args['form'], 'name') );
			$this->user_model->setEmail( getValue($args['form'], 'email') );
			$this->user_model->setSenha( getValue($args['form'], 'password') );
			$this->user_model->setSubdominio( getValue($args['form'], 'subdomain') );
			$this->user_model->setCreci( getValue($args['form'], 'creci') );
			$this->user_model->setTelefone( getValue($args['form'], 'phone') );
			//$this->user_model->setCreciUF( getValue($args['form'], 'creci_uf') );
			//$this->user_model->setDataNascimento( getValue($args['form'], 'data_nascimento') );
			//$this->user_model->setEstado( getValue($args['form'], 'estado') );
			//$this->user_model->setCidade( getValue($args['form'], 'cidade') );
			//$this->user_model->setBairro( getValue($args['form'], 'bairro') );
			//$this->user_model->setCelular( getValue($args['form'], 'celular') );

			$this->user_model->setTermo( 'S' );
			$subdomain = $this->user_model->getSubdominio();		
			if($this->user_model->save() == 1){
				echo 1;
			} else {
				echo 0;
			}
		} else {
			echo 0;
		}
	}

	/*
	* @method login()
	* carrega tela de login
	*/
	public function login(){
		VIEW('home/login');
	}

	function doLogin(){
		$args['filter'] = $this->input->post();

		MODEL('login');

		$subdomain = (getSubDomain() == null) ? domain() : getSubDomain();

		$affected = $this->login_model->validate(
			$subdomain,
			$args['filter']['email'],
			$args['filter']['password']
		);

		if($affected > 0){

			$args['data'] = $this->login_model->get_user_data(
				$args['filter']['subdominio'],
				$args['filter']['email'],
				$args['filter']['password']
			);
			// Carrega library
			$this->load->library('session');
			// Array com os dados
			$arrSession = array(

				'logado' 	=> true,
				'id'=>getValue($args['data'][0], 'id'),
				'tipousuario'=>getValue($args['data'][0], 'tipousuario'),
				'subdominio'=>getValue($args['data'][0], 'subdominio'),
				'nome_completo'=>getValue($args['data'][0], 'nome_completo'),
				'creci'=>getValue($args['data'][0], 'creci'),
				'creci_uf'=>getValue($args['data'][0], 'creci_uf'),
				'data_nascimento'=>getValue($args['data'][0], 'data_nascimento'),
				'email'=>getValue($args['data'][0], 'email'),
				'senha'=>getValue($args['data'][0], 'senha'),
				'estado'=>getValue($args['data'][0], 'estado'),
				'cidade'=>getValue($args['data'][0], 'cidade'),
				'bairro'=>getValue($args['data'][0], 'bairro'),
				'telefone'=>getValue($args['data'][0], 'telefone'),
				'celular'=>getValue($args['data'][0], 'celular'),
				'termo'=>getValue($args['data'][0], 'termo'),
				'data_registro'=>getValue($args['data'][0], 'data_registro'),
				'visible'=>getValue($args['data'][0], 'visible'),
				'mydomain' => getValue($args['data'][0], 'subdominio')
			);
			// Cria sessão
			$this->session->set_userdata($arrSession);
			header('location: '. getValue($args['data'][0], 'subdominio') . '/dashboard/');
		} else {
			redirect('home/login');
		}
	}

	function doLogout(){
		$arrSession = array(

				'logado'=> $this->session->userdata('logado'),
				'id'=> $this->session->userdata('id'),
				'tipousuario'=> $this->session->userdata('tipousuario'),
				'subdominio'=> $this->session->userdata('subdominio'),
				'nome_completo'=> $this->session->userdata('nome_completo'),
				'creci'=> $this->session->userdata('creci'),
				'creci_uf'=> $this->session->userdata('creci_uf'),
				'data_nascimento'=> $this->session->userdata('data_nascimento'),
				'email'=> $this->session->userdata('email'),
				'senha'=> $this->session->userdata('senha'),
				'estado'=> $this->session->userdata('estado'),
				'cidade'=> $this->session->userdata('cidade'),
				'bairro'=> $this->session->userdata('bairro'),
				'telefone'=> $this->session->userdata('telefone'),
				'celular'=> $this->session->userdata('celular'),
				'termo'=> $this->session->userdata('termo'),
				'data_registro'=> $this->session->userdata('data_registro'),
				'visible'=> $this->session->userdata('visible'),
				'mydomain'=> $this->session->userdata('mydomain'),
			);
			// Cria sessão
			$this->session->unset_userdata($arrSession);
			redirect();
	}


	public function gotItem(){
		$this->load->model('login_model');
		if($this->input->post('subdomain')){
			$affected = $this->login_model->gotItem($this->input->post('subdomain'), null);
			echo $affected;
		} 
		else if($this->input->post('email')){
			$affected = $this->login_model->gotItem(null, $this->input->post('email'));
			echo $affected;
		} else {
			echo 0;
		}
	}

	public function details($id, $featured = null){

		if($this->session->userdata('profile_picture') != ""){
			$args['foto_perfil'] = PROFILEPICTURELINK .'profile_'.$this->session->userdata('userid').'/'.$this->session->userdata('profile_picture');	
		} else {
			$args['foto_perfil'] = PROFILEPICTURELINK .'default.jpg';	
		}

		if($featured == "featured"){
			$args['imovel'] = $this->externo->getFeaturedByID($id);
			VIEW('home/details', $args);
		} else {
			$args['imovel'] = $this->externo->getPropertyBySYSID($id);

			if($this->session->userdata('profile_picture') != ""){
				$args['foto_perfil'] = PROFILEPICTURELINK .'profile_'.$this->session->userdata('userid').'/'.$this->session->userdata('profile_picture');	
			} else {
				$args['foto_perfil'] = PROFILEPICTURELINK .'default.jpg';	
			}

			$args['sysid'] = $id;

			VIEW('home/details_no_featured', $args);	
		}		
	}

	public function destruct(){
		$OwnerInfo = array(
			'owner' => true,
			'total' => $this->session->userdata('total'),
			'subdominio' => $this->session->userdata('subdominio'),
			'nome_completo' => $this->session->userdata('nome_completo'),
			'creci' => $this->session->userdata('creci'),
			'email' => $this->session->userdata('email'),
			'telefone' => $this->session->userdata('telefone'),
		);
		$this->session->unset_userdata($OwnerInfo);
	}
}
