<?php
/**
 * User: fernandoalls
 * Date: 02/02/15
 * Time: 19:26
 */
class Rets extends CI_Controller{


    public function update1(){
        set_time_limit(0);
        if(file_exists( THIRDPARTY .'phprets/connect.php' ) ){
            require THIRDPARTY .'phprets/connect.php';


            $rets = new phRETS;
            $rets->AddHeader("RETS-Version", "RETS/1.0");
            $rets->AddHeader("User-Agent", $retsUserAgent);
            $connect = $rets->Connect( $retsUrlConnect, $retsUsername, $retsPassword, $retsUserAgentPassword);
            if($connect) {
                getResultByResource($rets, 'Property', 1);
            } else {
                $error = $rets->Error();
                print_r($error);
            }
        } else {
            echo "<h1>Rets cannot be updated because we cant find this app.</h1>";
        }
    }

    public function update2(){
        set_time_limit(0);
        if(file_exists( THIRDPARTY .'phprets/connect.php' ) ){
            require THIRDPARTY .'phprets/connect.php';


            $rets = new phRETS;
            $rets->AddHeader("RETS-Version", "RETS/1.0");
            $rets->AddHeader("User-Agent", $retsUserAgent);
            $connect = $rets->Connect( $retsUrlConnect, $retsUsername, $retsPassword, $retsUserAgentPassword);
            if($connect) {
                getResultByResource($rets, 'Property', 2);
            } else {
                $error = $rets->Error();
                print_r($error);
            }
        } else {
            echo "<h1>Rets cannot be updated because we cant find this app.</h1>";
        }
    }

    public function update3(){
        set_time_limit(0);
        if(file_exists( THIRDPARTY .'phprets/connect.php' ) ){
            require THIRDPARTY .'phprets/connect.php';


            $rets = new phRETS;
            $rets->AddHeader("RETS-Version", "RETS/1.0");
            $rets->AddHeader("User-Agent", $retsUserAgent);
            $connect = $rets->Connect( $retsUrlConnect, $retsUsername, $retsPassword, $retsUserAgentPassword);
            if($connect) {
                //getResultByResource($rets, 'Property', 1);
                //getResultByResource($rets, 'Property', 2);
                getResultByResource($rets, 'Property', 3);
                //getResultByResource($rets, 'Property', 4);
                //getResultByResource($rets, 'Property', 5);
                //getResultByResource($rets, 'Property', 6);
                //getResultByResource($rets, 'Property', 7);
                //getResultByResource($rets, 'Property', 8);
            } else {
                $error = $rets->Error();
                print_r($error);
            }
        } else {
            echo "<h1>Rets cannot be updated because we cant find this app.</h1>";
        }
    }

    public function update4(){
        set_time_limit(0);
        if(file_exists( THIRDPARTY .'phprets/connect.php' ) ){
            require THIRDPARTY .'phprets/connect.php';


            $rets = new phRETS;
            $rets->AddHeader("RETS-Version", "RETS/1.0");
            $rets->AddHeader("User-Agent", $retsUserAgent);
            $connect = $rets->Connect( $retsUrlConnect, $retsUsername, $retsPassword, $retsUserAgentPassword);
            if($connect) {
                getResultByResource($rets, 'Property', 4);
            } else {
                $error = $rets->Error();
                print_r($error);
            }
        } else {
            echo "<h1>Rets cannot be updated because we cant find this app.</h1>";
        }
    }

    public function update5(){
        set_time_limit(0);
        if(file_exists( THIRDPARTY .'phprets/connect.php' ) ){
            require THIRDPARTY .'phprets/connect.php';


            $rets = new phRETS;
            $rets->AddHeader("RETS-Version", "RETS/1.0");
            $rets->AddHeader("User-Agent", $retsUserAgent);
            $connect = $rets->Connect( $retsUrlConnect, $retsUsername, $retsPassword, $retsUserAgentPassword);
            if($connect) {
                getResultByResource($rets, 'Property', 5);
            } else {
                $error = $rets->Error();
                print_r($error);
            }
        } else {
            echo "<h1>Rets cannot be updated because we cant find this app.</h1>";
        }
    }

    public function update6(){
        set_time_limit(0);
        if(file_exists( THIRDPARTY .'phprets/connect.php' ) ){
            require THIRDPARTY .'phprets/connect.php';


            $rets = new phRETS;
            $rets->AddHeader("RETS-Version", "RETS/1.0");
            $rets->AddHeader("User-Agent", $retsUserAgent);
            $connect = $rets->Connect( $retsUrlConnect, $retsUsername, $retsPassword, $retsUserAgentPassword);
            if($connect) {
                getResultByResource($rets, 'Property', 6);
            } else {
                $error = $rets->Error();
                print_r($error);
            }
        } else {
            echo "<h1>Rets cannot be updated because we cant find this app.</h1>";
        }
    }

    public function update7(){
        set_time_limit(0);
        if(file_exists( THIRDPARTY .'phprets/connect.php' ) ){
            require THIRDPARTY .'phprets/connect.php';


            $rets = new phRETS;
            $rets->AddHeader("RETS-Version", "RETS/1.0");
            $rets->AddHeader("User-Agent", $retsUserAgent);
            $connect = $rets->Connect( $retsUrlConnect, $retsUsername, $retsPassword, $retsUserAgentPassword);
            if($connect) {
                getResultByResource($rets, 'Property', 7);
            } else {
                $error = $rets->Error();
                print_r($error);
            }
        } else {
            echo "<h1>Rets cannot be updated because we cant find this app.</h1>";
        }
    }

    public function update8(){
        set_time_limit(0);
        if(file_exists( THIRDPARTY .'phprets/connect.php' ) ){
            require THIRDPARTY .'phprets/connect.php';


            $rets = new phRETS;
            $rets->AddHeader("RETS-Version", "RETS/1.0");
            $rets->AddHeader("User-Agent", $retsUserAgent);
            $connect = $rets->Connect( $retsUrlConnect, $retsUsername, $retsPassword, $retsUserAgentPassword);
            if($connect) {
                getResultByResource($rets, 'Property', 8);
            } else {
                $error = $rets->Error();
                print_r($error);
            }
        } else {
            echo "<h1>Rets cannot be updated because we cant find this app.</h1>";
        }
    }

    public function update9(){
        set_time_limit(0);
        if(file_exists( THIRDPARTY .'phprets/connect.php' ) ){
            require THIRDPARTY .'phprets/connect.php';


            $rets = new phRETS;
            $rets->AddHeader("RETS-Version", "RETS/1.0");
            $rets->AddHeader("User-Agent", $retsUserAgent);
            $connect = $rets->Connect( $retsUrlConnect, $retsUsername, $retsPassword, $retsUserAgentPassword);
            if($connect) {
                getResultByResource($rets, 'OpenHouse', 13);
            } else {
                $error = $rets->Error();
                print_r($error);
            }
        } else {
            echo "<h1>Rets cannot be updated because we cant find this app.</h1>";
        }
    }
}