'use strict';
(function($) {
	var validate = new validateObject();
	validate.__construct();
	var adminList = new adminList();
	adminList.__construct();

})(jQuery);

function validateObject(){
	self = this;

	self.__construct = function(){
		self.registerValidation();
		self.submit();
	}

	self.registerValidation = function(){
		$( "#admin-form" ).validate({
			errorClass: "state-error",
			validClass: "state-success",
			errorElement: "em",

			rules: {
				firstname: { required: true },
				username: { required: true },
				creci: { required: true },
				email: { required: true },
				subdomain: { required: true, url: true },
				password: { required: true }
			},
			messages:{
				firstname: { required: 'Campo Obrigatório' },
				username: { required: 'Campo Obrigatório' },
				creci: { required: 'Campo Obrigatório' },
				email: { required: 'Campo Obrigatório' },
				subdomain: { required: 'Campo Obrigatório' },
				password: { required: 'Campo Obrigatório' }

			},
			highlight: function(element, errorClass, validClass) {
				$(element).closest('.field').addClass(errorClass).removeClass(validClass);
			},
			unhighlight: function(element, errorClass, validClass) {
				$(element).closest('.field').removeClass(errorClass).addClass(validClass);
			},
			errorPlacement: function(error, element) {
				if (element.is(":radio") || element.is(":checkbox")) {
				     element.closest('.option-group').after(error);
				} else {
				     error.insertAfter(element.parent());
				}
			}
		});
	}

	self.submit = function(){
		$('form').submit(function(){
			var dados = jQuery( this ).serialize();
			jQuery.ajax({
				type: "POST",
				url: siteurl + "home/doRegister",
				data: dados,
				success: function( data )
				{
					if(data == 1){
						
						var domain = $('#subdomain').val().split('.');
						window.location = domain[0] + '.miamielite.com.br/home/login/'
					} else {	
						alert('Erro ao realizar o cadastro!');
					}
				}
			});
			return false;
		});
	}
}