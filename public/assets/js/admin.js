/**
 * Created by fernandoalls on 03/02/15.
 */

function redirectTo(to){
    window.location = to;
}

function save(mlnumber, userid){
	$.post( siteurl + '/dashboard/ajax_set_property', { mlnumber: mlnumber, id: userid } )
	.done(function(data){
		if(data == 1){
			$('#btn_'+mlnumber).attr('disabled', 'disabled');
			alert('Salvo em sua vitrine!');
		} 
		else if(data == 0){
			alert('Erro ao salvar!');
		}
	})
}

function removeItem(mlnumber, userid){
	$.post( siteurl + '/dashboard/ajax_remove_property', { mlnumber: mlnumber, id: userid } )
	.done(function(data){
		//console.log(data)
		if(data == 1){
			$('#mlnumber_'+mlnumber).remove();
			alert('Removido de sua vitrine!');
		} 
		else if(data == 0){
			alert('Erro ao remover!');
		}
	})
}